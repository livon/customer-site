/*Auxilliary methods*/
var crypto = require('crypto');
var nodemailer = require('nodemailer');

//define functions
var nCrypt = function(str, key) {
	var ckey = parseInt(key) || parseInt(sails.config.lv.PassKey);
	var hash = '';

	for (var i = 0; i < str.length; i++) {
		var cVal = parseInt(str.charCodeAt(i));
		//console.log(cVal);
		var cLen = cVal.toString().length;
		hash += (ckey ^ (cVal << (ckey.toString().length))) + '-';
	}

	return hash.substring(0, hash.length - 1);
}

var dCrypt = function(hash, key) {
	var dkey = parseInt(key) || parseInt(sails.config.lv.PassKey);
	var hAry = hash.split('-');
	var pwd = '';

	for (var i = 0; i < hAry.length; i++) {
		pwd += String.fromCharCode(((dkey ^ parseInt(hAry[i])) >> dkey.toString().length));
	}

	return pwd;
}


//set up nodemailer
if (typeof sails != 'undefined') {

var transporter = nodemailer.createTransport({
	service: "Zoho",
	auth: {
		user: sails.config.lv.ZohoUserName,
		pass: dCrypt(sails.config.lv.ZohoHash)
	}
});

// verify connection configuration
transporter.verify(function(error, success) {
   if (error) {
        console.log(error);
   } else {
        console.log('Server is ready to take our messages');
   }
});

}

module.exports = {
	md5 : function(str) {
		return crypto.createHash('md5').update(str).digest('hex');
	},
	mail : function(from, to, bcc, replyTo, subject, html, callback) {
		transporter.sendMail({
			from: from,
			to: to,
			bcc: bcc,
			replyTo: replyTo,
			subject: subject,
			html: html
		}, function(err, res) {
			if (err) {
				console.log(err);
				callback(err);
			} else {
				console.log('Message sent');
				callback('success');
			}
		});
	},
	nCrypt : nCrypt,
	dCrypt : dCrypt,
	createOrderNum : function() {
		return Math.floor(Math.random()*90000) + 10000;
	}
}




