-- MySQL dump 10.13  Distrib 5.7.20, for macos10.12 (x86_64)
--
-- Host: localhost    Database: livon
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Security_log`
--

DROP TABLE IF EXISTS `Security_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Security_log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `User_Id` int(11) NOT NULL,
  `Action` varchar(100) NOT NULL,
  `Action_datetime` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Security_log`
--

LOCK TABLES `Security_log` WRITE;
/*!40000 ALTER TABLE `Security_log` DISABLE KEYS */;
INSERT INTO `Security_log` VALUES (1,2,'login','2017-12-21 18:48:07'),(2,2,'login','2017-12-21 19:05:54'),(3,2,'login','2017-12-21 19:19:36'),(4,2,'login','2017-12-21 19:26:02'),(5,2,'login','2017-12-22 21:19:14');
/*!40000 ALTER TABLE `Security_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `box_vendors`
--

DROP TABLE IF EXISTS `box_vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `box_vendors` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Vendor` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `box_vendors`
--

LOCK TABLES `box_vendors` WRITE;
/*!40000 ALTER TABLE `box_vendors` DISABLE KEYS */;
INSERT INTO `box_vendors` VALUES (1,'USPS'),(2,'AMZN(Reused)');
/*!40000 ALTER TABLE `box_vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliveries`
--

DROP TABLE IF EXISTS `deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveries` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Order_num` int(11) NOT NULL,
  `Delivery_datetime` datetime NOT NULL,
  `Created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `Updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveries`
--

LOCK TABLES `deliveries` WRITE;
/*!40000 ALTER TABLE `deliveries` DISABLE KEYS */;
INSERT INTO `deliveries` VALUES (3,3,'2017-12-13 21:49:00','2017-12-11 21:49:20',1,'2017-12-11 21:49:20',1),(4,4,'2017-12-14 16:56:00','2017-12-11 21:56:27',1,'2017-12-11 21:56:27',1),(5,73,'2017-12-25 19:28:00','2017-12-21 19:29:03',2,'2017-12-21 19:29:03',2);
/*!40000 ALTER TABLE `deliveries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_statuses`
--

DROP TABLE IF EXISTS `order_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_statuses` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Status` varchar(50) NOT NULL,
  `Position` int(11) NOT NULL DEFAULT '-1',
  `Next_status_id` int(11) DEFAULT '-1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_statuses`
--

LOCK TABLES `order_statuses` WRITE;
/*!40000 ALTER TABLE `order_statuses` DISABLE KEYS */;
INSERT INTO `order_statuses` VALUES (1,'New',1,2),(2,'In Progress',2,3),(3,'Packaged',3,4),(4,'Shipped',4,5),(5,'Delivered',5,-1),(6,'Canceled',-1,-1),(7,'Refunded',-1,-1),(8,'Delayed',-1,-1);
/*!40000 ALTER TABLE `order_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tracking`
--

DROP TABLE IF EXISTS `order_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tracking` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Order_num` int(11) NOT NULL,
  `Order_status_id` int(11) NOT NULL,
  `Comment` varchar(140) DEFAULT NULL,
  `Created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `Updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tracking`
--

LOCK TABLES `order_tracking` WRITE;
/*!40000 ALTER TABLE `order_tracking` DISABLE KEYS */;
INSERT INTO `order_tracking` VALUES (1,1,2,'','2017-12-03 19:02:17',1,'2017-12-14 09:33:23',1),(2,2,5,'','2017-12-03 19:02:17',1,'2017-12-06 18:01:04',1),(3,3,5,'','2017-12-03 19:02:17',1,'2017-12-11 21:50:34',1),(4,4,5,'','2017-12-03 19:02:17',1,'2017-12-11 21:56:27',1),(5,5,7,'','2017-12-04 21:20:36',1,'2017-12-06 18:01:49',1),(6,59,1,'','2017-12-17 12:58:39',1,'2017-12-17 12:58:39',1),(7,60,1,'','2017-12-17 13:46:44',1,'2017-12-17 13:46:44',1),(8,61,1,'','2017-12-17 13:54:22',1,'2017-12-17 13:54:22',1),(9,62,1,'','2017-12-17 14:00:43',1,'2017-12-17 14:00:43',1),(10,63,1,'','2017-12-17 14:21:29',1,'2017-12-17 14:21:29',1),(11,64,1,'','2017-12-17 14:25:04',1,'2017-12-17 14:25:04',1),(12,65,1,'','2017-12-17 14:29:24',1,'2017-12-17 14:29:24',1),(13,66,1,'','2017-12-17 17:12:21',1,'2017-12-17 17:12:21',1),(14,67,1,'','2017-12-17 17:18:31',1,'2017-12-17 17:18:31',1),(15,68,1,'','2017-12-17 17:25:02',1,'2017-12-17 17:25:02',1),(16,69,1,'','2017-12-17 17:37:42',1,'2017-12-17 17:37:42',1),(17,70,1,'','2017-12-17 18:01:12',1,'2017-12-17 18:01:12',1),(18,71,1,'','2017-12-17 18:03:07',1,'2017-12-17 18:03:07',1),(19,72,1,'','2017-12-17 18:03:38',1,'2017-12-17 18:03:38',1),(20,73,5,'','2017-12-17 18:11:16',1,'2017-12-21 19:29:03',2),(21,74,1,'','2017-12-17 18:13:09',1,'2017-12-17 18:13:09',1),(22,75,4,'','2017-12-17 18:17:53',1,'2017-12-21 19:20:44',2),(23,76,4,'','2017-12-17 18:19:27',1,'2017-12-21 19:10:15',2);
/*!40000 ALTER TABLE `order_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tracking_history`
--

DROP TABLE IF EXISTS `order_tracking_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tracking_history` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Order_num` int(11) NOT NULL,
  `Order_status_id` int(11) NOT NULL,
  `Comment` varchar(140) DEFAULT NULL,
  `Created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `Updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tracking_history`
--

LOCK TABLES `order_tracking_history` WRITE;
/*!40000 ALTER TABLE `order_tracking_history` DISABLE KEYS */;
INSERT INTO `order_tracking_history` VALUES (1,5,1,'','2017-12-05 15:44:08',1,'2017-12-05 15:44:08',1),(2,4,1,'','2017-12-05 15:44:08',1,'2017-12-05 15:44:08',1),(3,3,1,'','2017-12-05 15:44:08',1,'2017-12-05 15:44:08',1),(4,2,1,'','2017-12-05 15:44:08',1,'2017-12-05 15:44:08',1),(5,1,1,'','2017-12-05 15:44:08',1,'2017-12-05 15:44:08',1),(8,3,2,'','2017-12-05 18:01:53',1,'2017-12-05 18:01:53',1),(9,4,2,'','2017-12-05 18:21:49',1,'2017-12-05 18:21:49',1),(10,2,2,'','2017-12-05 18:22:45',1,'2017-12-05 18:22:45',1),(11,5,1,'','2017-12-06 09:46:18',1,'2017-12-06 09:46:18',1),(12,5,2,'','2017-12-06 10:43:02',1,'2017-12-06 10:43:02',1),(13,5,1,'','2017-12-06 10:47:11',1,'2017-12-06 10:47:11',1),(14,5,2,'','2017-12-06 10:48:51',1,'2017-12-06 10:48:51',1),(15,5,1,'','2017-12-06 10:58:44',1,'2017-12-06 10:58:44',1),(16,3,1,'','2017-12-06 11:00:44',1,'2017-12-06 11:00:44',1),(17,5,2,'','2017-12-06 11:02:43',1,'2017-12-06 11:02:43',1),(18,5,1,'','2017-12-06 11:07:04',1,'2017-12-06 11:07:04',1),(19,5,2,'','2017-12-06 11:08:23',1,'2017-12-06 11:08:23',1),(20,5,1,'','2017-12-06 11:12:33',1,'2017-12-06 11:12:33',1),(21,5,2,'','2017-12-06 12:52:26',1,'2017-12-06 12:52:26',1),(22,5,1,'','2017-12-06 12:58:19',1,'2017-12-06 12:58:19',1),(23,5,2,'','2017-12-06 13:13:59',1,'2017-12-06 13:13:59',1),(24,5,1,'','2017-12-06 13:25:58',1,'2017-12-06 13:25:58',1),(25,5,2,'','2017-12-06 13:28:14',1,'2017-12-06 13:28:14',1),(26,5,1,'','2017-12-06 13:29:32',1,'2017-12-06 13:29:32',1),(27,5,2,'','2017-12-06 13:33:12',1,'2017-12-06 13:33:12',1),(28,5,1,'','2017-12-06 13:33:14',1,'2017-12-06 13:33:14',1),(29,5,2,'','2017-12-06 16:41:22',1,'2017-12-06 16:41:22',1),(30,5,3,'','2017-12-06 16:41:33',1,'2017-12-06 16:41:33',1),(31,5,4,'','2017-12-06 16:41:38',1,'2017-12-06 16:41:38',1),(32,5,5,'','2017-12-06 16:41:54',1,'2017-12-06 16:41:54',1),(33,5,1,'','2017-12-06 17:57:46',1,'2017-12-06 17:57:46',1),(34,5,5,'','2017-12-06 17:57:51',1,'2017-12-06 17:57:51',1),(35,5,1,'','2017-12-06 17:58:09',1,'2017-12-06 17:58:09',1),(36,5,2,'','2017-12-06 17:58:15',1,'2017-12-06 17:58:15',1),(37,5,3,'','2017-12-06 17:58:23',1,'2017-12-06 17:58:23',1),(38,5,4,'','2017-12-06 17:58:30',1,'2017-12-06 17:58:30',1),(39,5,5,'','2017-12-06 17:58:38',1,'2017-12-06 17:58:38',1),(40,3,8,'','2017-12-06 17:59:49',1,'2017-12-06 17:59:49',1),(41,3,1,'','2017-12-06 18:00:06',1,'2017-12-06 18:00:06',1),(42,3,2,'','2017-12-06 18:00:18',1,'2017-12-06 18:00:18',1),(43,3,1,'','2017-12-06 18:00:25',1,'2017-12-06 18:00:25',1),(44,1,2,'','2017-12-06 18:00:29',1,'2017-12-06 18:00:29',1),(45,3,2,'','2017-12-06 18:00:33',1,'2017-12-06 18:00:33',1),(46,1,1,'','2017-12-06 18:00:37',1,'2017-12-06 18:00:37',1),(47,1,1,'','2017-12-06 18:00:37',1,'2017-12-06 18:00:37',1),(48,3,1,'','2017-12-06 18:00:38',1,'2017-12-06 18:00:38',1),(49,1,2,'','2017-12-06 18:00:40',1,'2017-12-06 18:00:40',1),(50,3,2,'','2017-12-06 18:00:41',1,'2017-12-06 18:00:41',1),(51,1,1,'','2017-12-06 18:00:42',1,'2017-12-06 18:00:42',1),(52,1,1,'','2017-12-06 18:00:42',1,'2017-12-06 18:00:42',1),(53,3,1,'','2017-12-06 18:00:43',1,'2017-12-06 18:00:43',1),(54,3,2,'','2017-12-06 18:00:47',1,'2017-12-06 18:00:47',1),(55,2,3,'','2017-12-06 18:00:50',1,'2017-12-06 18:00:50',1),(56,2,4,'','2017-12-06 18:00:54',1,'2017-12-06 18:00:54',1),(57,2,5,'','2017-12-06 18:01:04',1,'2017-12-06 18:01:04',1),(58,5,6,'','2017-12-06 18:01:38',1,'2017-12-06 18:01:38',1),(59,5,7,'','2017-12-06 18:01:49',1,'2017-12-06 18:01:49',1),(60,1,2,'','2017-12-06 18:23:09',1,'2017-12-06 18:23:09',1),(61,1,1,'','2017-12-06 18:23:14',1,'2017-12-06 18:23:14',1),(62,1,2,'','2017-12-06 18:23:25',1,'2017-12-06 18:23:25',1),(63,1,1,'','2017-12-06 18:23:49',1,'2017-12-06 18:23:49',1),(64,3,3,'','2017-12-08 18:31:23',1,'2017-12-08 18:31:23',1),(65,1,2,'','2017-12-08 19:30:15',1,'2017-12-08 19:30:15',1),(66,1,1,'','2017-12-08 19:30:20',1,'2017-12-08 19:30:20',1),(67,1,2,'','2017-12-08 19:36:51',1,'2017-12-08 19:36:51',1),(68,1,1,'','2017-12-08 19:36:53',1,'2017-12-08 19:36:53',1),(69,3,4,'','2017-12-09 00:20:43',1,'2017-12-09 00:20:43',1),(70,3,3,'','2017-12-09 00:24:38',1,'2017-12-09 00:24:38',1),(71,3,4,'','2017-12-09 00:25:22',1,'2017-12-09 00:25:22',1),(72,3,3,'','2017-12-09 00:28:13',1,'2017-12-09 00:28:13',1),(73,3,4,'','2017-12-09 00:28:42',1,'2017-12-09 00:28:42',1),(74,3,3,'','2017-12-09 00:37:57',1,'2017-12-09 00:37:57',1),(75,3,4,'','2017-12-09 00:38:25',1,'2017-12-09 00:38:25',1),(76,3,3,'','2017-12-09 00:47:48',1,'2017-12-09 00:47:48',1),(77,3,4,'','2017-12-09 00:48:42',1,'2017-12-09 00:48:42',1),(78,3,3,'','2017-12-09 00:58:08',1,'2017-12-09 00:58:08',1),(79,3,4,'','2017-12-09 00:58:29',1,'2017-12-09 00:58:29',1),(80,3,3,'','2017-12-09 01:50:03',1,'2017-12-09 01:50:03',1),(81,3,4,'','2017-12-09 01:50:40',1,'2017-12-09 01:50:40',1),(82,3,3,'','2017-12-09 01:52:35',1,'2017-12-09 01:52:35',1),(83,3,4,'','2017-12-09 01:53:06',1,'2017-12-09 01:53:06',1),(84,3,2,'','2017-12-09 01:54:53',1,'2017-12-09 01:54:53',1),(85,3,4,'','2017-12-09 01:54:58',1,'2017-12-09 01:54:58',1),(86,3,4,'','2017-12-09 01:55:01',1,'2017-12-09 01:55:01',1),(87,3,4,'','2017-12-09 01:55:02',1,'2017-12-09 01:55:02',1),(88,3,4,'','2017-12-09 01:55:02',1,'2017-12-09 01:55:02',1),(89,3,3,'','2017-12-09 01:55:17',1,'2017-12-09 01:55:17',1),(90,3,4,'','2017-12-09 01:55:43',1,'2017-12-09 01:55:43',1),(91,3,3,'','2017-12-09 01:59:55',1,'2017-12-09 01:59:55',1),(92,3,4,'','2017-12-09 02:00:30',1,'2017-12-09 02:00:30',1),(93,3,3,'','2017-12-09 02:10:08',1,'2017-12-09 02:10:08',1),(94,3,4,'','2017-12-09 02:10:45',1,'2017-12-09 02:10:45',1),(95,3,3,'','2017-12-09 02:14:16',1,'2017-12-09 02:14:16',1),(96,3,4,'','2017-12-09 02:14:51',1,'2017-12-09 02:14:51',1),(97,3,3,'','2017-12-09 02:17:26',1,'2017-12-09 02:17:26',1),(98,3,4,'','2017-12-09 02:17:53',1,'2017-12-09 02:17:53',1),(99,3,3,'','2017-12-09 02:21:34',1,'2017-12-09 02:21:34',1),(100,3,4,'','2017-12-09 02:22:00',1,'2017-12-09 02:22:00',1),(101,3,3,'','2017-12-09 02:23:50',1,'2017-12-09 02:23:50',1),(102,3,4,'','2017-12-09 02:24:18',1,'2017-12-09 02:24:18',1),(103,3,3,'','2017-12-09 02:26:27',1,'2017-12-09 02:26:27',1),(104,3,4,'','2017-12-09 02:26:53',1,'2017-12-09 02:26:53',1),(105,3,3,'','2017-12-09 02:28:37',1,'2017-12-09 02:28:37',1),(106,3,4,'','2017-12-09 02:29:00',1,'2017-12-09 02:29:00',1),(107,3,3,'','2017-12-09 10:07:32',1,'2017-12-09 10:07:32',1),(108,3,4,'','2017-12-09 10:19:15',1,'2017-12-09 10:19:15',1),(109,3,3,'','2017-12-09 11:09:37',1,'2017-12-09 11:09:37',1),(110,3,4,'','2017-12-09 11:10:05',1,'2017-12-09 11:10:05',1),(111,3,3,'','2017-12-09 11:13:22',1,'2017-12-09 11:13:22',1),(112,3,4,'','2017-12-09 11:13:51',1,'2017-12-09 11:13:51',1),(113,3,3,'','2017-12-09 11:15:44',1,'2017-12-09 11:15:44',1),(114,3,4,'','2017-12-09 11:16:53',1,'2017-12-09 11:16:53',1),(115,3,3,'','2017-12-09 13:32:35',1,'2017-12-09 13:32:35',1),(116,3,1,'','2017-12-09 13:33:55',1,'2017-12-09 13:33:55',1),(117,3,2,'','2017-12-09 13:44:25',1,'2017-12-09 13:44:25',1),(118,3,3,'','2017-12-09 13:44:49',1,'2017-12-09 13:44:49',1),(119,3,4,'','2017-12-09 13:46:52',1,'2017-12-09 13:46:52',1),(120,3,3,'','2017-12-09 13:48:23',1,'2017-12-09 13:48:23',1),(121,3,4,'','2017-12-09 13:50:11',1,'2017-12-09 13:50:11',1),(122,3,3,'','2017-12-10 14:52:32',1,'2017-12-10 14:52:32',1),(123,3,4,'','2017-12-10 14:53:06',1,'2017-12-10 14:53:06',1),(124,3,2,'','2017-12-10 15:34:42',1,'2017-12-10 15:34:42',1),(125,3,3,'','2017-12-10 15:34:51',1,'2017-12-10 15:34:51',1),(126,3,4,'','2017-12-10 15:35:30',1,'2017-12-10 15:35:30',1),(127,3,3,'','2017-12-10 15:37:04',1,'2017-12-10 15:37:04',1),(128,3,4,'','2017-12-10 15:37:33',1,'2017-12-10 15:37:33',1),(129,3,3,'','2017-12-10 15:39:24',1,'2017-12-10 15:39:24',1),(130,3,4,'','2017-12-10 15:40:56',1,'2017-12-10 15:40:56',1),(131,3,3,'','2017-12-10 15:41:36',1,'2017-12-10 15:41:36',1),(132,3,4,'','2017-12-10 15:42:48',1,'2017-12-10 15:42:48',1),(133,3,3,'','2017-12-10 15:45:05',1,'2017-12-10 15:45:05',1),(134,3,4,'','2017-12-10 15:46:34',1,'2017-12-10 15:46:34',1),(135,3,3,'','2017-12-10 16:15:41',1,'2017-12-10 16:15:41',1),(136,3,4,'','2017-12-10 16:16:15',1,'2017-12-10 16:16:15',1),(137,3,3,'','2017-12-10 16:18:00',1,'2017-12-10 16:18:00',1),(138,3,4,'','2017-12-10 16:18:45',1,'2017-12-10 16:18:45',1),(139,3,3,'','2017-12-10 16:27:01',1,'2017-12-10 16:27:01',1),(140,3,4,'','2017-12-10 16:29:16',1,'2017-12-10 16:29:16',1),(141,3,3,'','2017-12-10 16:31:15',1,'2017-12-10 16:31:15',1),(142,3,4,'','2017-12-10 16:34:10',1,'2017-12-10 16:34:10',1),(143,3,3,'','2017-12-10 16:35:22',1,'2017-12-10 16:35:22',1),(144,3,4,'','2017-12-10 16:35:55',1,'2017-12-10 16:35:55',1),(145,3,3,'','2017-12-10 16:37:44',1,'2017-12-10 16:37:44',1),(146,3,4,'','2017-12-10 16:38:39',1,'2017-12-10 16:38:39',1),(147,3,3,'','2017-12-10 16:41:45',1,'2017-12-10 16:41:45',1),(148,3,4,'','2017-12-10 16:42:20',1,'2017-12-10 16:42:20',1),(149,3,3,'','2017-12-10 16:43:01',1,'2017-12-10 16:43:01',1),(150,3,4,'','2017-12-10 16:43:27',1,'2017-12-10 16:43:27',1),(151,3,3,'','2017-12-10 16:44:36',1,'2017-12-10 16:44:36',1),(152,3,4,'','2017-12-10 16:45:00',1,'2017-12-10 16:45:00',1),(153,3,3,'','2017-12-10 16:59:36',1,'2017-12-10 16:59:36',1),(154,3,4,'','2017-12-10 17:00:08',1,'2017-12-10 17:00:08',1),(155,3,3,'','2017-12-10 17:00:46',1,'2017-12-10 17:00:46',1),(156,3,4,'','2017-12-10 17:01:15',1,'2017-12-10 17:01:15',1),(157,3,3,'','2017-12-10 17:25:57',1,'2017-12-10 17:25:57',1),(158,3,4,'','2017-12-10 17:26:27',1,'2017-12-10 17:26:27',1),(159,3,3,'','2017-12-10 17:27:58',1,'2017-12-10 17:27:58',1),(160,3,4,'','2017-12-10 17:28:28',1,'2017-12-10 17:28:28',1),(161,3,3,'','2017-12-10 17:31:05',1,'2017-12-10 17:31:05',1),(162,3,4,'','2017-12-10 17:31:33',1,'2017-12-10 17:31:33',1),(163,3,3,'','2017-12-10 17:39:28',1,'2017-12-10 17:39:28',1),(164,3,4,'','2017-12-10 17:40:14',1,'2017-12-10 17:40:14',1),(165,3,3,'','2017-12-10 17:53:28',1,'2017-12-10 17:53:28',1),(166,3,4,'','2017-12-10 17:54:01',1,'2017-12-10 17:54:01',1),(167,3,3,'','2017-12-10 17:55:09',1,'2017-12-10 17:55:09',1),(168,3,4,'','2017-12-10 17:55:27',1,'2017-12-10 17:55:27',1),(169,3,3,'','2017-12-10 17:56:42',1,'2017-12-10 17:56:42',1),(170,3,4,'','2017-12-10 17:57:04',1,'2017-12-10 17:57:04',1),(171,3,3,'','2017-12-10 17:58:28',1,'2017-12-10 17:58:28',1),(172,3,4,'','2017-12-10 17:58:51',1,'2017-12-10 17:58:51',1),(173,3,3,'','2017-12-10 18:35:37',1,'2017-12-10 18:35:37',1),(174,3,4,'','2017-12-10 18:36:12',1,'2017-12-10 18:36:12',1),(175,3,3,'','2017-12-10 19:56:31',1,'2017-12-10 19:56:31',1),(176,3,4,'','2017-12-10 19:57:23',1,'2017-12-10 19:57:23',1),(177,1,2,'','2017-12-10 20:32:29',1,'2017-12-10 20:32:29',1),(178,1,1,'','2017-12-10 20:32:50',1,'2017-12-10 20:32:50',1),(179,4,3,'','2017-12-10 20:40:05',1,'2017-12-10 20:40:05',1),(180,1,2,'','2017-12-11 15:28:18',1,'2017-12-11 15:28:18',1),(181,1,1,'','2017-12-11 15:28:20',1,'2017-12-11 15:28:20',1),(182,1,2,'','2017-12-11 15:29:27',1,'2017-12-11 15:29:27',1),(183,1,1,'','2017-12-11 15:29:29',1,'2017-12-11 15:29:29',1),(184,3,5,'','2017-12-11 21:42:11',1,'2017-12-11 21:42:11',1),(185,3,4,'','2017-12-11 21:44:40',1,'2017-12-11 21:44:40',1),(186,3,5,'','2017-12-11 21:46:00',1,'2017-12-11 21:46:00',1),(187,4,2,'','2017-12-11 21:47:32',1,'2017-12-11 21:47:32',1),(188,4,3,'','2017-12-11 21:47:36',1,'2017-12-11 21:47:36',1),(189,3,4,'','2017-12-11 21:47:50',1,'2017-12-11 21:47:50',1),(190,3,5,'','2017-12-11 21:49:20',1,'2017-12-11 21:49:20',1),(191,3,2,'','2017-12-11 21:50:04',1,'2017-12-11 21:50:04',1),(192,3,5,'','2017-12-11 21:50:34',1,'2017-12-11 21:50:34',1),(193,4,1,'','2017-12-11 21:51:47',1,'2017-12-11 21:51:47',1),(194,4,2,'','2017-12-11 21:51:51',1,'2017-12-11 21:51:51',1),(195,4,3,'','2017-12-11 21:51:55',1,'2017-12-11 21:51:55',1),(196,4,4,'','2017-12-11 21:53:30',1,'2017-12-11 21:53:30',1),(197,4,5,'','2017-12-11 21:56:27',1,'2017-12-11 21:56:27',1),(198,1,2,'','2017-12-14 09:33:23',1,'2017-12-14 09:33:23',1),(199,57,1,'','2017-12-17 12:22:07',1,'2017-12-17 12:22:07',1),(200,59,1,'','2017-12-17 12:58:39',1,'2017-12-17 12:58:39',1),(201,60,1,'','2017-12-17 13:46:44',1,'2017-12-17 13:46:44',1),(202,61,1,'','2017-12-17 13:54:22',1,'2017-12-17 13:54:22',1),(203,62,1,'','2017-12-17 14:00:43',1,'2017-12-17 14:00:43',1),(204,63,1,'','2017-12-17 14:21:29',1,'2017-12-17 14:21:29',1),(205,64,1,'','2017-12-17 14:25:04',1,'2017-12-17 14:25:04',1),(206,65,1,'','2017-12-17 14:29:24',1,'2017-12-17 14:29:24',1),(207,66,1,'','2017-12-17 17:12:21',1,'2017-12-17 17:12:21',1),(208,67,1,'','2017-12-17 17:18:31',1,'2017-12-17 17:18:31',1),(209,68,1,'','2017-12-17 17:25:02',1,'2017-12-17 17:25:02',1),(210,69,1,'','2017-12-17 17:37:42',1,'2017-12-17 17:37:42',1),(211,70,1,'','2017-12-17 18:01:12',1,'2017-12-17 18:01:12',1),(212,71,1,'','2017-12-17 18:03:07',1,'2017-12-17 18:03:07',1),(213,72,1,'','2017-12-17 18:03:38',1,'2017-12-17 18:03:38',1),(214,73,1,'','2017-12-17 18:11:16',1,'2017-12-17 18:11:16',1),(215,74,1,'','2017-12-17 18:13:09',1,'2017-12-17 18:13:09',1),(216,75,1,'','2017-12-17 18:17:53',1,'2017-12-17 18:17:53',1),(217,76,1,'','2017-12-17 18:19:27',1,'2017-12-17 18:19:27',1),(218,75,2,'','2017-12-17 21:01:23',1,'2017-12-17 21:01:23',1),(219,75,1,'','2017-12-17 21:01:28',1,'2017-12-17 21:01:28',1),(220,76,2,'','2017-12-18 10:31:53',1,'2017-12-18 10:31:53',1),(221,76,1,'','2017-12-18 10:31:57',1,'2017-12-18 10:31:57',1),(222,76,2,'','2017-12-18 19:34:59',1,'2017-12-18 19:34:59',1),(223,76,1,'','2017-12-18 19:35:04',1,'2017-12-18 19:35:04',1),(224,76,2,'','2017-12-18 19:35:10',1,'2017-12-18 19:35:10',1),(225,75,2,'','2017-12-21 19:06:41',2,'2017-12-21 19:06:41',2),(226,76,3,'','2017-12-21 19:07:37',2,'2017-12-21 19:07:37',2),(227,76,4,'','2017-12-21 19:10:15',2,'2017-12-21 19:10:15',2),(228,75,3,'','2017-12-21 19:20:04',2,'2017-12-21 19:20:04',2),(229,75,4,'','2017-12-21 19:20:44',2,'2017-12-21 19:20:44',2),(230,73,2,'','2017-12-21 19:26:11',2,'2017-12-21 19:26:11',2),(231,73,3,'','2017-12-21 19:26:16',2,'2017-12-21 19:26:16',2),(232,73,4,'','2017-12-21 19:27:17',2,'2017-12-21 19:27:17',2),(233,73,5,'','2017-12-21 19:29:03',2,'2017-12-21 19:29:03',2);
/*!40000 ALTER TABLE `order_tracking_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `Order_Num` int(11) NOT NULL AUTO_INCREMENT,
  `Order_datetime` datetime NOT NULL,
  `Ship_first_name` varchar(50) NOT NULL,
  `Ship_last_name` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Product` varchar(75) NOT NULL,
  `Size` varchar(100) NOT NULL,
  `Qty` int(11) NOT NULL,
  `Product_price` decimal(13,4) NOT NULL,
  `Promo_code` varchar(50) DEFAULT NULL,
  `Promo_desc` varchar(50) DEFAULT NULL,
  `Promo_price` decimal(13,4) DEFAULT NULL,
  `Subtotal` decimal(13,4) NOT NULL,
  `Shipping` decimal(13,4) NOT NULL,
  `Taxes_fees` decimal(13,4) NOT NULL,
  `Amt_paid` decimal(13,4) NOT NULL,
  `Card_charged` varchar(50) DEFAULT NULL,
  `Ship_address` varchar(75) NOT NULL,
  `Ship_city` varchar(75) NOT NULL,
  `Ship_state` char(2) NOT NULL,
  `Ship_country` varchar(25) NOT NULL DEFAULT 'USA',
  `Ship_zip` char(5) NOT NULL,
  `Bill_first_name` varchar(50) NOT NULL,
  `Bill_last_name` varchar(50) NOT NULL,
  `Bill_address` varchar(75) NOT NULL,
  `Bill_city` varchar(75) NOT NULL,
  `Bill_state` char(2) NOT NULL,
  `Bill_country` varchar(25) NOT NULL DEFAULT 'USA',
  `Bill_zip` char(5) NOT NULL,
  `Created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `Updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `Is_personalized` char(1) NOT NULL DEFAULT 'N',
  `Product_long` varchar(75) NOT NULL,
  `Product_id` int(11) NOT NULL,
  `Promo_type` varchar(50) DEFAULT NULL,
  `Promo_shipping` decimal(13,4) DEFAULT NULL,
  `Is_payment_collected` char(1) NOT NULL DEFAULT 'Y',
  `Comment` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Order_Num`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2017-11-11 16:56:18','Elena','Rodriguez','jabarisalih@gmail.com','Liv On Chocolate Cinnamon','820g (20 servings) | 29oz',2,59.9900,'ELENAISASWEETHEART','5% Off + Free Shipping',57.0000,113.9900,0.0000,13.4000,127.3800,'MasterCard 3839','1625 Main St, Apt 602','Houston','TX','USA','77002','Elena','Rodriguez','1625 Main St, Apt 602','Houston','TX','USA','77002','2017-11-14 16:16:33',1,'2017-11-14 16:16:33',1,'N','Liv On Chocolate Cinnamon healthy protein powder',4,'Combo',0.0000,'Y',NULL),(2,'2017-11-12 14:00:00','Elena','Rodriguez','jabarisalih@gmail.com','Liv On Chocolate Cinnamon','820g (20 servings) | 29oz',1,59.9900,'ELENAISASWEETHEART','Free Shipping',NULL,59.9900,0.0000,6.0000,55.9900,'MasterCard 3839','1625 Main St, Apt 602','Houston','TX','USA','77002','Elena','Rodriguez','1625 Main St, Apt 602','Houston','TX','USA','77002','2017-11-14 16:16:33',1,'2017-11-14 16:16:33',1,'N','Liv On Chocolate Cinnamon healthy protein powder',4,'Shipping',0.0000,'Y',NULL),(3,'2017-11-13 14:00:00','Elena','Rodriguez','jabarisalih@gmail.com','Liv On Chocolate Cinnamon','820g (20 servings) | 29oz',1,59.9900,'ELENAISASWEETHEART','10% Off',53.9900,53.9900,6.9500,6.0000,66.9400,'MasterCard 3839','1625 Main St, Apt 602','Houston','TX','USA','77002','Elena','Rodriguez','1625 Main St, Apt 602','Houston','TX','USA','77002','2017-11-14 16:16:34',1,'2017-11-14 16:16:34',1,'N','Liv On Chocolate Cinnamon healthy protein powder',4,'Price',NULL,'Y',NULL),(4,'2017-11-12 14:00:00','Elena','Rodriguez','jabarisalih@gmail.com','Liv On Chocolate Cinnamon','820g (20 servings) | 29oz',1,59.9900,NULL,NULL,NULL,59.9900,6.9500,6.0000,72.9400,'MasterCard 3839','1625 Main St, Apt 602','Houston','TX','USA','77002','Elena','Rodriguez','1625 Main St, Apt 602','Houston','TX','USA','77002','2017-11-14 16:19:12',1,'2017-11-14 16:19:12',1,'N','Liv On Chocolate Cinnamon healthy protein powder',4,'',NULL,'Y',NULL),(5,'2017-12-04 16:00:00','Elena','Rodriguez','jabarisalih@gmail.com','Liv On Vanilla Cinnamon','750g (20 servings) | 27oz',1,59.9900,NULL,NULL,NULL,59.9900,6.9500,6.0000,72.9400,'MasterCard 3839','1625 Main St, Apt 602','Houston','TX','USA','77002','Elena','Rodriguez','1625 Main St, Apt 602','Houston','TX','USA','77002','2017-12-04 21:16:14',1,'2017-12-04 21:16:14',1,'N','Liv On Vanilla Cinnamon healthy protein powder',6,'',NULL,'Y',NULL),(55,'2017-12-17 11:37:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,'JUSTFORMELANIE','FREE Sample Pack',0.0000,0.0000,6.9500,0.0000,6.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 11:38:31',1,'2017-12-17 11:38:31',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,'Price',NULL,'Y','First Order!'),(56,'2017-12-17 11:43:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,'JUSTFORMELANIE','FREE Sample Pack',0.0000,0.0000,6.9500,0.0000,6.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 11:47:29',1,'2017-12-17 11:47:29',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,'Price',NULL,'Y','Order Test!'),(57,'2017-12-17 12:20:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Vanilla Cinnamon Sample Pack','185g (5 servings) | 6.5oz',1,15.0000,'JUSTFORMELANIE','FREE Sample Pack',0.0000,0.0000,6.9500,0.0000,6.9500,'visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 12:22:07',1,'2017-12-17 12:22:07',1,'Y','Liv On Vanilla Cinnamon Sample Pack healthy protein powder',5,'Price',NULL,'Y',NULL),(58,'2017-12-17 12:52:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Vanilla Cinnamon Sample Pack','185g (5 servings) | 6.5oz',1,15.0000,'JUSTFORMELANIE','FREE Sample Pack',0.0000,0.0000,6.9500,0.0000,6.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 12:54:20',1,'2017-12-17 12:54:20',1,'Y','Liv On Vanilla Cinnamon Sample Pack healthy protein powder',5,'Price',NULL,'Y','Test!'),(59,'2017-12-17 12:56:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Vanilla Cinnamon Sample Pack','185g (5 servings) | 6.5oz',1,15.0000,'JUSTFORMELANIE','FREE Sample Pack',0.0000,0.0000,6.9500,0.0000,6.9500,'Visa 4242','10 Oak Court','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court','Houston','TX','USA','77006','2017-12-17 12:58:39',1,'2017-12-17 12:58:39',1,'Y','Liv On Vanilla Cinnamon Sample Pack healthy protein powder',5,'Price',NULL,'Y','Test!!'),(60,'2017-12-17 13:44:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Chocolate Cinnamon','820g (20 servings) | 29oz',1,59.9900,'JUSTFORMELANIE','FREE Large Container',0.0000,0.0000,0.0000,0.0000,0.0000,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 13:46:44',1,'2017-12-17 13:46:44',1,'Y','Liv On Chocolate Cinnamon healthy protein powder',4,'Combo',NULL,'Y','Almost there!'),(61,'2017-12-17 13:51:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,'JUSTFORMELANIE','10% Off',13.5000,13.5000,6.9500,3.0000,23.4500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 13:54:22',1,'2017-12-17 13:54:22',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,'Price',NULL,'Y','Another test'),(62,'2017-12-17 13:59:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Matcha Green Tea Sample Pack','138g (5 servings) | 8oz',1,15.0000,'JUSTFORMELANIE','10% Off',13.5000,13.5000,6.9500,3.0000,23.4500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 14:00:43',1,'2017-12-17 14:00:43',1,'Y','Liv On Matcha Green Tea Sample Pack healthy protein powder',10,'Price',NULL,'Y','Test'),(63,'2017-12-17 14:20:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Matcha Green Tea','780g (20 servings) | 27.5oz',1,59.9900,'JUSTFORMELANIE','5% Off',56.9900,56.9900,6.9500,5.0000,68.9400,'Visa 4242','10 Oak Court 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court 5304','Houston','TX','USA','77006','2017-12-17 14:21:29',1,'2017-12-17 14:21:29',1,'Y','Liv On Matcha Green Tea healthy protein powder',8,'Price',NULL,'Y','Test!'),(64,'2017-12-17 14:22:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,'JUSTFORMELANIE','5% Off',14.2500,14.2500,6.9500,2.0000,23.3000,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 14:25:04',1,'2017-12-17 14:25:04',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,'Price',NULL,'Y','Test!'),(65,'2017-12-17 14:27:00','Melanie','Bornarski','jabarisalih@gmail.com','Liv On Vanilla Cinnamon Sample Pack','185g (5 servings) | 6.5oz',1,15.0000,'JUSTFORMELANIE','10% Off',13.5000,13.5000,6.9500,3.0000,23.4500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Melanie','Bornarski','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 14:29:24',1,'2017-12-17 14:29:24',1,'Y','Liv On Vanilla Cinnamon Sample Pack healthy protein powder',5,'Price',NULL,'Y','Test'),(66,'2017-12-17 17:11:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Matcha Green Tea','780g (20 servings) | 27.5oz',1,59.9900,NULL,NULL,NULL,60.0000,0.0000,0.0000,60.0000,NULL,'10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 17:12:21',1,'2017-12-17 17:12:21',1,'Y','Liv On Matcha Green Tea healthy protein powder',8,NULL,NULL,'Y',NULL),(67,'2017-12-17 17:16:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,'JUSTFORGAELLE','10% Off',13.5000,13.5000,6.9500,3.0000,23.4500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 17:18:31',1,'2017-12-17 17:18:31',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,'Price',NULL,'Y',NULL),(68,'2017-12-17 17:23:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Vanilla Cinnamon Sample Pack','185g (5 servings) | 6.5oz',1,15.0000,NULL,NULL,NULL,15.0000,6.9500,3.0000,24.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 17:25:02',1,'2017-12-17 17:25:02',1,'Y','Liv On Vanilla Cinnamon Sample Pack healthy protein powder',5,NULL,NULL,'Y',NULL),(69,'2017-12-17 17:35:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,NULL,NULL,NULL,15.0000,6.9500,3.0000,24.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 17:37:42',1,'2017-12-17 17:37:42',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,NULL,NULL,'Y',NULL),(70,'2017-12-17 17:59:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,NULL,NULL,NULL,15.0000,6.9500,3.0000,24.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 18:01:12',1,'2017-12-17 18:01:12',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,NULL,NULL,'Y',NULL),(71,'2017-12-17 18:02:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Vanilla Cinnamon Sample Pack','185g (5 servings) | 6.5oz',1,15.0000,NULL,NULL,NULL,15.0000,6.9500,3.0000,24.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 18:03:07',1,'2017-12-17 18:03:07',1,'N','Liv On Vanilla Cinnamon Sample Pack healthy protein powder',5,NULL,NULL,'Y',NULL),(72,'2017-12-17 18:02:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,NULL,NULL,NULL,15.0000,6.9500,3.0000,24.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 18:03:38',1,'2017-12-17 18:03:38',1,'N','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,NULL,NULL,'Y',NULL),(73,'2017-12-17 18:09:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,NULL,NULL,NULL,15.0000,6.9500,3.0000,24.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 18:11:16',1,'2017-12-17 18:11:16',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,NULL,NULL,'Y',NULL),(74,'2017-12-17 18:09:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Vanilla Cinnamon Sample Pack','185g (5 servings) | 6.5oz',1,15.0000,NULL,NULL,NULL,15.0000,6.9500,3.0000,24.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 18:13:09',1,'2017-12-17 18:13:09',1,'N','Liv On Vanilla Cinnamon Sample Pack healthy protein powder',5,NULL,NULL,'Y',NULL),(75,'2017-12-17 18:16:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Chocolate Cinnamon Sample Pack','235g (5 servings) | 8.5oz',1,15.0000,'JUSTFORGAELLE','10% Off',13.5000,13.5000,6.9500,3.0000,23.4500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 18:17:53',1,'2017-12-17 18:17:53',1,'Y','Liv On Chocolate Cinnamon Sample Pack healthy protein powder',3,'Price',NULL,'Y',NULL),(76,'2017-12-17 18:18:00','Gaelle','Dufour','jabarisalih@gmail.com','Liv On Vanilla Cinnamon Sample Pack','185g (5 servings) | 6.5oz',1,15.0000,NULL,NULL,NULL,15.0000,6.9500,3.0000,24.9500,'Visa 4242','10 Oak Court Apt 5304','Houston','TX','USA','77006','Gaelle','Dufour','10 Oak Court Apt 5304','Houston','TX','USA','77006','2017-12-17 18:19:27',1,'2017-12-17 18:19:27',1,'Y','Liv On Vanilla Cinnamon Sample Pack healthy protein powder',5,NULL,NULL,'Y',NULL);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_types`
--

DROP TABLE IF EXISTS `payment_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_types` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_types`
--

LOCK TABLES `payment_types` WRITE;
/*!40000 ALTER TABLE `payment_types` DISABLE KEYS */;
INSERT INTO `payment_types` VALUES (1,'Stripe'),(2,'PayPal'),(3,'Cash'),(4,'Square'),(5,'Product Promotion'),(6,'NA - Free Sample');
/*!40000 ALTER TABLE `payment_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pickup_locations`
--

DROP TABLE IF EXISTS `pickup_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pickup_locations` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Address` varchar(75) NOT NULL,
  `City` varchar(75) NOT NULL,
  `State` char(2) NOT NULL,
  `Zip` char(5) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pickup_locations`
--

LOCK TABLES `pickup_locations` WRITE;
/*!40000 ALTER TABLE `pickup_locations` DISABLE KEYS */;
INSERT INTO `pickup_locations` VALUES (1,'Kitchen 205','205 W Crosstimbers Rd.','Houston','TX','77018'),(2,'USPS Hadley St.','1500 Hadley St.','Houston','TX','77002'),(3,'Home Office','10 Oak Ct.','Houston','TX','77006');
/*!40000 ALTER TABLE `pickup_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promo_types`
--

DROP TABLE IF EXISTS `promo_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo_types` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo_types`
--

LOCK TABLES `promo_types` WRITE;
/*!40000 ALTER TABLE `promo_types` DISABLE KEYS */;
INSERT INTO `promo_types` VALUES (1,'Price'),(2,'Shipping'),(3,'Combo');
/*!40000 ALTER TABLE `promo_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipment_boxes`
--

DROP TABLE IF EXISTS `shipment_boxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipment_boxes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Box_vendor_id` int(11) NOT NULL,
  `Size` varchar(50) NOT NULL,
  `Price` decimal(13,4) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment_boxes`
--

LOCK TABLES `shipment_boxes` WRITE;
/*!40000 ALTER TABLE `shipment_boxes` DISABLE KEYS */;
INSERT INTO `shipment_boxes` VALUES (1,1,'CshnMlr 8.5\"x12\"',1.5900),(2,1,'8.5\" x 12\" bubble',1.7900),(3,1,'Priority Mail Shoe Box 7.5\" x 5.125\" x 14.375\"',0.0000),(4,1,'6\" x 10\" cushion',1.1900),(5,1,'Regional Box A1',0.0000),(6,1,'15\" x 12\" x 10\" box',3.3900),(7,2,'Whatever Fits',0.0000);
/*!40000 ALTER TABLE `shipment_boxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipment_emails`
--

DROP TABLE IF EXISTS `shipment_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipment_emails` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Shipment_id` int(11) NOT NULL,
  `From_email` varchar(100) NOT NULL,
  `To_email` varchar(100) NOT NULL,
  `To_name` varchar(100) NOT NULL,
  `Sent_by` varchar(50) NOT NULL,
  `Sent_datetime` datetime NOT NULL,
  `Created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `Updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment_emails`
--

LOCK TABLES `shipment_emails` WRITE;
/*!40000 ALTER TABLE `shipment_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipment_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipment_services`
--

DROP TABLE IF EXISTS `shipment_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipment_services` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Shipment_vendor_id` int(11) NOT NULL,
  `Service` varchar(50) NOT NULL,
  `Avg_delivery_days` int(11) NOT NULL DEFAULT '1',
  `Display_text` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment_services`
--

LOCK TABLES `shipment_services` WRITE;
/*!40000 ALTER TABLE `shipment_services` DISABLE KEYS */;
INSERT INTO `shipment_services` VALUES (1,1,'First-Class Parcel Service',3,'3 Days - Standard'),(2,1,'PM 2-Day',2,'2 Days - Priority'),(3,4,'Delivery',1,'1 Day - Local Delivery');
/*!40000 ALTER TABLE `shipment_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipment_vendors`
--

DROP TABLE IF EXISTS `shipment_vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipment_vendors` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Vendor` varchar(50) NOT NULL,
  `Avg_delivery_days` int(11) NOT NULL DEFAULT '1',
  `Tracking_link` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment_vendors`
--

LOCK TABLES `shipment_vendors` WRITE;
/*!40000 ALTER TABLE `shipment_vendors` DISABLE KEYS */;
INSERT INTO `shipment_vendors` VALUES (1,'USPS',3,'https://tools.usps.com/go/TrackConfirmAction?tRef=fullpage&tLc=2&text28777=&tLabels='),(2,'FedEx',4,NULL),(3,'UPS',5,NULL),(4,'Liv On',2,NULL),(5,'favor',1,NULL),(6,'Amazon',2,NULL);
/*!40000 ALTER TABLE `shipment_vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipments`
--

DROP TABLE IF EXISTS `shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipments` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Order_num` int(11) NOT NULL,
  `Shipment_vendor` varchar(25) NOT NULL,
  `Tracking_num` varchar(75) DEFAULT NULL,
  `Shipment_datetime` datetime NOT NULL,
  `Pickup_address` varchar(75) NOT NULL,
  `Pickup_city` varchar(75) NOT NULL,
  `Pickup_state` char(2) NOT NULL,
  `Pickup_zip` char(5) NOT NULL,
  `Pickup_country` varchar(25) NOT NULL DEFAULT 'USA',
  `Expected_arrival_day` varchar(10) NOT NULL,
  `Expected_arrival_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `Updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `Shipment_box` varchar(75) NOT NULL,
  `Shipment_box_price` decimal(13,4) NOT NULL,
  `Shipment_weight_lbs` decimal(13,4) NOT NULL,
  `Shipment_weight_kgs` decimal(13,4) NOT NULL,
  `Shipment_cost` decimal(13,4) NOT NULL,
  `Pickup_name` varchar(75) NOT NULL,
  `Shipment_service` varchar(75) NOT NULL,
  `Shipment_vendor_id` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipments`
--

LOCK TABLES `shipments` WRITE;
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` VALUES (33,3,'USPS','9500 1110 0585 7229 1224 11','2017-12-10 19:57:00','205 W Crosstimbers Rd.','Houston','TX','77018','USA','Wednesday','2017-12-13','2017-12-10 19:57:23',1,'2017-12-10 19:57:23',1,'USPS Regional Box A1',0.0000,2.5500,1.1567,9.8800,'Kitchen 205','First-Class Parcel Service',1),(34,4,'USPS','9505 5110 0584 7177 0893 03','2017-12-12 08:00:00','205 W Crosstimbers Rd.','Houston','TX','77018','USA','Thursday','2017-12-14','2017-12-11 21:53:30',1,'2017-12-11 21:53:30',1,'USPS Regional Box A1',0.0000,2.6000,1.1793,9.7500,'Kitchen 205','PM 2-Day',1),(35,76,'USPS','9500 1110 0585 7229 1224 04','2017-12-21 19:10:00','205 W Crosstimbers Rd.','Houston','TX','77018','USA','Sunday','2017-12-24','2017-12-21 19:10:15',2,'2017-12-21 19:10:15',2,'USPS Regional Box A1',0.0000,2.5000,1.1340,5.3400,'Kitchen 205','First-Class Parcel Service',1),(36,75,'USPS','9500 1110 0585 7229 1224 04','2017-12-21 19:20:00','205 W Crosstimbers Rd.','Houston','TX','77018','USA','Sunday','2017-12-24','2017-12-21 19:20:44',2,'2017-12-21 19:20:44',2,'USPS Regional Box A1',0.0000,2.6000,1.1793,5.9500,'Kitchen 205','First-Class Parcel Service',1),(37,73,'USPS','9500 1110 0585 7229 1224 04','2017-12-21 19:27:00','205 W Crosstimbers Rd.','Houston','TX','77018','USA','Sunday','2017-12-24','2017-12-21 19:27:17',2,'2017-12-21 19:27:17',2,'USPS Regional Box A1',0.0000,0.6000,0.2722,5.3500,'Kitchen 205','First-Class Parcel Service',1);
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(100) NOT NULL,
  `First_name` varchar(75) NOT NULL,
  `Last_name` varchar(75) NOT NULL,
  `Username` varchar(75) DEFAULT NULL,
  `Hash` varchar(250) NOT NULL,
  `Created_at` datetime NOT NULL,
  `Created_by` int(11) NOT NULL,
  `Updated_at` datetime NOT NULL,
  `Updated_by` int(11) NOT NULL,
  `Is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'customer.service@alwayslivon.com','Liv On','~','svc-livon','nohash','2017-12-18 23:32:31',1,'2017-12-18 23:32:31',1,1),(2,'jabari@alwayslivon.com','Jabari','Holloway',NULL,'a4227138fb876e510de24faa5ed679ccf1e3f0ef02ab48164ac387b204c4aad0','2017-12-18 23:32:31',2,'2017-12-18 23:32:31',2,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-22 21:24:26
