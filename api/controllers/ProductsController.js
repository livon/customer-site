/* Products Controller */

var fs = require('fs');
var path = require('path');
var request = require('request');
var https = require('https');
var csv = require('ya-csv');
//var nodemailer = require('nodemailer');
var helper = require(__dirname + '/../../helpers/helper');
var jsonql = require(__dirname + '/../../helpers/jsonql');
var appDir = path.dirname(require.main.filename);


module.exports = {
	index: function(req, res) {
		//sails.logger.info('[html] url: ' + req.url);

		//return list of offered products in reverse order
		var path = __dirname + '/../../content/products.json';
		 		fs.readFile(path, function(err, data) {
 					if (err) {
 						//sails.logger.error('[stories] error: ' + err);
 						console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
					//console.log("test");
					//console.log(data.toString());
 					var products = JSON.parse(data);
 					var pList = [];

 					for (var i = products.length - 1; i >= 0; i--) {
 						if (products[i].isListed === true) {
 							pList.push(products[i]);
 							//console.log(products[i].isNew);
 						}
 					}

 					//console.log(JSON.stringify(pList));
 					//console.log(pList.length);

 					res.view({products: pList});

 				});
	
		//res.view();

	},
	details: function(req, res) {

			//get the product by id and return data to view
		var prodId = req.param('name');

 		if (prodId) {
				prodId = prodId.toLowerCase();
 				//sails.logger.info('[products] url: ' + req.url);
 				var path = __dirname + '/../../content/products.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						//sails.logger.error('[stories] error: ' + err);
 						console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
					//console.log("test");
					//console.log(data.toString());
 					var products = JSON.parse(data);
 					var product = jsonql.findById(products, prodId);
 					product.title = 'Liv On ' +  product.name + ' | ' + product.description;

 					//console.log(JSON.stringify(story));

 					//check if story is available - if so 

 					var recExists = !jsonql.isEmptyObject(product);

 					if (recExists) {
 						res.view(product);
 					} else {
 						//sails.logger.error('[products] error: no record found; url: ' + req.url);
 						res.notFound();
 					}

 				});
 		} else {
 			//sails.logger.error('[products] error: no product id provided; url: ' + req.url);
 			res.notFound();
 		}
	},
	cartAdd: function(req, res) {

		//get the strings package by id and return data to view
		var prodId = req.param('name');

		console.log(JSON.stringify(req.allParams()));

		

 		if (prodId) {
				prodId = prodId.toLowerCase();
 				//sails.logger.info('[products] url: ' + req.url);
 				var path = __dirname + '/../../content/products.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						//sails.logger.error('[stories] error: ' + err);
 						console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
					//console.log("test");
					//console.log(data.toString());
 					var products = JSON.parse(data);
 					var product = jsonql.findById(products, prodId);


 					//console.log(JSON.stringify(story));

 					//check if story is available - if so 

 					var recExists = !jsonql.isEmptyObject(product);

 					if (recExists) {
 						if (req.param('flavor') && req.param('size')  && req.param('sizeId')
								 && req.param('qty') && req.param('name')) {

								//define params
								var ip = req.ip;
								var name = req.param('name');
								var flavor = req.param('flavor');
								var price =  product.price;
								var qty = req.param('qty');
								var size = req.param('size');
								var sizeId = req.param('sizeId');
								var date = new Date();
								var time = date.getHours() + ":" ; 
								 time += date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
								
								date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();

								for (var i = 0; i < product.sizes.length; i++) {
									console.log(product.sizes[i].id);
										console.log(sizeId);

									if (product.sizes[i].id.toString() === sizeId.toString()) {
										price = product.sizes[i].price;

										break;
									}
								}


							    path = __dirname + '/../../content/orders.csv';
								var w = csv.createCsvFileWriter(path, {'flags': 'a'});
								var data = [ip, name, flavor, size, parseInt(price), parseInt(qty), date, time];

								w.writeRecord(data);
								//log order in orders.csv

		}
 						//product.title = 'Rockstar Strings | Checkout'
 						//res.view(product);
 					} else {
 						//sails.logger.error('[products] error: no record found; url: ' + req.url);
 						res.notFound();
 					}

 				});
 		} else {
 			//sails.logger.error('[products] error: no product id provided; url: ' + req.url);
 			res.notFound();
 		}

	},
	get: function(req, res) {
	if (req.wantsJSON) {
		//sails.logger.info('[achievements - find] url: ' + req.url);
	  	//console.log('find');
 		//console.log(req.param('name'));
 		var prodId = req.param('id'); //integer

 		if (prodId) {
 				var path = __dirname + '/../../content/products.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						//sails.logger.error('[achievements - find] error: ' + err);
 						//console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
 					var products = JSON.parse(data);
 					var product = jsonql.findByProdId(products, prodId);

 					 //this should have all info needed
 					//for an order confirmation (NOT TRUE). it must include all the info 
 					//needed to complete the transaction and forward the prod id & 
 					//promo information to the orderConfirmation action
 					//the product id, the product name, the product description 

 					//check the url query for promo id and promo=true

 					//does it make security sense to check if the promo was applied on the
 					//client side? I don't think so.


 					var recExists = !jsonql.isEmptyObject(product);

 					if (recExists) {
 						
 						//check for promo in url query
 						if (req.query.promo && req.query.promoId) {
 							//check to see if promo is avalaible
 							//if so, add info to product (product.validUrlPromo)
 							//update view to reflect that promo has been applied
 							if (product.promotion) {

 								var thisPromo = null;

 								for (var i = 0; i < product.promotion.promoCodes.length; i++) {
 									if (req.query.promoId === product.promotion.promoCodes[i].id) {
 										thisPromo = product.promotion.promoCodes[i];
 										break;
 									}
 								}

 								if (thisPromo) {
 									if (thisPromo.valid === true) {
 										product.validUrlPromo = {id: thisPromo.id,
 																 code: thisPromo.code, 
 															  	 price: product.promotion.promoPrice,
 															  	 img: thisPromo.pImg };
 									}
 								} else {
 									product.validUrlPromo = null;
 								}
 							}
 						}



 						res.json({error: "api to be added"});
 					} else {
 						res.json({error: "not found"});
 					}

 					

 				});
 		} else {
 			//sails.logger.error('[achievements - find] error: no story id provided; url: ' + req.url);
 		}
 	  } else {
 	  		//sails.logger.error('[achievements - find: json required] url: ' + req.url + '; msg: html request to json endpoint');
 	  }
	}
}