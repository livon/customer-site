/* Products Controller */

var fs = require('fs');
//var nodemailer = require('nodemailer');
var helper = require(__dirname + '/../../helpers/helper');
var jsonql = require(__dirname + '/../../helpers/jsonql');
var stripe = require("stripe")("sk_test_eN8nsPyGsciJJMM6TsvMmFif");
var db = require(__dirname + '/../../../livon_admin/helpers/db');
var mef = require('mef')('b037e2761813449e3eab9dbbb4ee17b45f731c72eb');
//var stripe = require("stripe")("sk_live_pCYn0T09KigehQnBN8ZsWxvR");
const cheerio = require('cheerio');



/*
* promoObj has the form {"id": "exampleId", "code": "exampleCode"}
* "code" is mandatory, "id" is optional. If "id" is specified,
* it will be used instead of "code"
*
*/
function checkForMultiProductPromos(promoObj, prodId, callback) {

	var path = __dirname + '/../../content/multi_product_promos.json';

		fs.readFile(path, function(err, data) {
			if (err) {
				//sails.logger.error('[stories] error: ' + err);
				console.log(err);
				res.serverError();
			}

			var promos = JSON.parse(data);

			var promo = null;

			if (promoObj.id) {

				promo = jsonql.findByPropertyVal(promos, 'id', promoObj.id);

			} else {
				//use promo code
				promo = jsonql.findByPropertyVal(promos, 'code', promoObj.code);

			}

			//console.log(JSON.stringify(promo));

			
			if (!jsonql.isEmptyObject(promo)) {
				//check promo scope
				if (promo.scope === 'site-wide') {

					promo.isValid = true;

					callback(null, promo);

				} else if (promo.scope === 'multi-product') {

					var prodRec = jsonql.findByPropertyVal(promo.products, 'prodId', prodId);

					if (!jsonql.isEmptyObject(prodRec)) {

						promo.isValid = true;
						callback(null, promo);
					} else {
						callback(null, {});
					}

				} else {

					let err = 'checkForMultiProductPromos Error: promo.scope';
					console.log(err);

					callback(err);
				}

			} else {
				callback(null, {});
			}

			

		});	
}


function checkForMultiProductPromosSync(promoObj, prodId) {


	var path = __dirname + '/../../content/multi_product_promos.json';

	var data = fs.readFileSync(path, 'utf-8');

	var promos = JSON.parse(data);

	var promo = null;

	if (promoObj.id) {

		promo = jsonql.findByPropertyVal(promos, 'id', promoObj.id);

	} else {
		//use promo code
		promo = jsonql.findByPropertyVal(promos, 'code', promoObj.code);

	}


	if (!jsonql.isEmptyObject(promo)) {
	//check promo scope
		if (promo.scope === 'site-wide') {

			promo.isValid = true;

			return promo;

		} else if (promo.scope === 'multi-product') {

			var prodRec = jsonql.findByPropertyVal(promo.products, 'prodId', prodId);

			if (!jsonql.isEmptyObject(prodRec)) {

				promo.isValid = true;
				return promo;
			} else {
				return null;
			}

		} else {

			return null;
		}

} else {
	return null;
}




}



module.exports = {
	index: function(req, res) {
		//sails.logger.info('[html] url: ' + req.url);
	
		res.view();

	},
	one: function(req, res) {


			//get the product by id and return data to view
		var prodId = req.param('product');

		//console.log(JSON.stringify(req.params));
			//console.log(req.query);

 		if (prodId) {
				prodId = prodId.toLowerCase();
 				//sails.logger.info('[products] url: ' + req.url);
 				var path = __dirname + '/../../content/products.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						//sails.logger.error('[stories] error: ' + err);
 						console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
					//console.log("test");
					//console.log(data.toString());
 					var products = JSON.parse(data);
 					var product = jsonql.findById(products, prodId);
 	

 					//console.log(JSON.stringify(story));

 					//check if story is available - if so 

 					var recExists = !jsonql.isEmptyObject(product);

 					if (recExists) {

 						product.title = 'Liv On ' +  product.name + ' | ' + product.description;

 						//check for promo in url query
 						if (req.query.promo && req.query.promoId) {


 							checkForMultiProductPromos({id: req.query.promoId}, product.prodId, function(err, promo) {

 								if (err) {
 									throw err;
 								}

 								var thisPromo = null;

 								if (!jsonql.isEmptyObject(promo)) {
 									thisPromo = promo;
 								} else {
 									if (product.promotion) {

 										for (var i = 0; i < product.promotion.promoCodes.length; i++) {
		 									if (req.query.promoId === product.promotion.promoCodes[i].id) {
		 										thisPromo = product.promotion.promoCodes[i];
		 										break;
		 									}
 										}
 									}
 								}

 								 if (thisPromo) {
 									if (thisPromo.valid === true) {

 										var isExpired = false;

 										//check for expiration
 										if (thisPromo.expires) {
 											//cast 
 											var expiresDate = new Date(thisPromo.expires);
 											var now = new Date();

 											//check if the current date time is >= 
 											if (now >= expiresDate) {
 												isExpired = true;
 											}
 											
 										}

 										if (!isExpired) {

 										var price = thisPromo.promoPrice !== 'undefined' ? thisPromo.promoPrice : product.promotion.promoPrice;

 										product.validUrlPromo = {id: thisPromo.id,
 																 code: thisPromo.code, 
 															  	 price: price,
 															  	 type: thisPromo.type
 															  	};

		 									if (thisPromo.pImg) {
		 										product.validUrlPromo.img = thisPromo.pImg;
		 									}

		 									if (thisPromo.combo) {
		 										product.validUrlPromo.combo = thisPromo.combo;
		 									}

		 									if (thisPromo.qty) {
		 										product.validUrlPromo.qty = thisPromo.qty;
		 									}

		 									if (thisPromo.desc) {
		 										product.validUrlPromo.desc = thisPromo.desc;
		 									}


 										} else {
 											product.validUrlPromo = null;
 										}

 									}


 								} else {
 									product.validUrlPromo = null;
 								}

 								res.view(product);

 							});


 						} else {
 						//console.log(JSON.stringify(product.validUrlPromo));
 						//console.log(JSON.stringify(product));
 						res.view(product);
 					  }
 					} else {
 						//sails.logger.error('[products] error: no record found; url: ' + req.url);
 						res.notFound();
 					}

 				});
 		} else {
 			//sails.logger.error('[products] error: no product id provided; url: ' + req.url);
 			res.notFound();
 		}
	},
	addPromotion: function(req, res) {
		//POST
		//checks to see if the entered promotion code exists and is still valid
		//if so, returns true. if not, returns false

		var prodId = req.param('prodId');//post the product id
		var codeEntered = req.param('promoCode') //check that text box isn't empty before submitting

		//console.log(JSON.stringify(req.params));


 		if (prodId && codeEntered) {
				prodId = prodId.toLowerCase();
				codeEntered = codeEntered.toUpperCase();
 				//sails.logger.info('[products] url: ' + req.url);

 				checkForMultiProductPromos({code: codeEntered}, prodId, function(err, mpPromo) {
 					if (err) {
 						throw err;
 					}

 					//console.log('add promotion: ' + JSON.stringify(mpPromo));

 					var thisPromo = null;
 					var promo = {};

					if (!jsonql.isEmptyObject(mpPromo)) {
						thisPromo = mpPromo;

						//check for validity
						if (thisPromo.valid === true) {

							var isExpired = false;

							//check for expiration
							if (thisPromo.expires) {
								//cast 
								var expiresDate = new Date(thisPromo.expires);
								var now = new Date();

								//check if the current date time is >= 
								if (now >= expiresDate) {
									isExpired = true;
								}
								
							}

							if (!isExpired) {

 								var price = thisPromo.promoPrice != 'undefined' ? thisPromo.promoPrice : product.promotion.promoPrice;

 								promo.id = thisPromo.id;
 								promo.price = price;
 								//promo.img = thisPromo.pImg;
 								promo.type = thisPromo.type;
 								promo.isValid = thisPromo.isValid;

 								if (thisPromo.type === 'combo') {
 									promo.combo = thisPromo.combo;
 								}

 								if (thisPromo.desc) {
 									promo.desc = thisPromo.desc;
 								}

 								if (thisPromo.qty) {
									promo.qty = thisPromo.qty;
								}

								if (thisPromo.pImg) {
									promo.img = thisPromo.pImg;
								}


								res.send(promo);

							} else {
								console.log('Expired promo');
								res.send('false');
							}

						   } else {
						   		console.log('Invalid promo');
						   	   res.send('false');
						   }

						}  else {
							//console.log('empty multi-product promo object. trying single product promos');
						//try single products (productsPath)
						 var productsPath = __dirname + '/../../content/products.json';

		 				fs.readFile(productsPath, function(err, data) {
		 					if (err) {
		 						//sails.logger.error('[stories] error: ' + err);
		 						console.log(err);
		 						res.send('false');
		 					}

		 					var products = JSON.parse(data);
		 					var product = jsonql.findByProdId(products, prodId);


		 					var recExists = !jsonql.isEmptyObject(product);

		 					if (recExists) {
		 						//check promo code
		 						if (product.promotion) {

		 							var validCode = false;
		 
		 							//loop promo codes
		 							for (var i = 0; i < product.promotion.promoCodes.length; i++) {
		 								if (codeEntered === product.promotion.promoCodes[i].code 
		 									&& product.promotion.promoCodes[i].valid === true) {

		 									validCode = true;
		 									thisPromo = product.promotion.promoCodes[i];
		 									break;
		 								}
		 							}
		 							//return promotion
		 							
		 							promo.isValid = validCode;

		 							if (thisPromo) {

		 								var price = thisPromo.promoPrice != 'undefined' ? thisPromo.promoPrice : product.promotion.promoPrice;

		 								promo.id = thisPromo.id;
		 								promo.price = price;
		 								promo.img = thisPromo.pImg;
		 								promo.type = thisPromo.type;

		 								if (thisPromo.type === 'combo') {
		 									promo.combo = thisPromo.combo;
		 								}

		 								if (thisPromo.desc) {
		 									promo.desc = thisPromo.desc;
		 								}

		 								if (thisPromo.qty) {
											promo.qty = thisPromo.qty;
										}

										if (thisPromo.pImg) {
											promo.img = thisPromo.pImg;
										}
		 							}

 							res.send(promo);
 						} else {
 							res.send('false');
 						}
 					} else {
 						
 						res.send('false');
 					}

 				});

			   }



 			});


 		} else {
 			
 			res.send('false'); 
 		}

	},
	submitPayment: function(req, res) {

		//submit the product Id and valid promotionId in order to determine the price
		//to charge the customer 
		//console.log(JSON.stringify(req.params));

		//console.log(req.param('prodId'));
		//console.log(req.param('promoId'));

		var tokenId = req.param('stripeToken');
		var prodId = req.param('prodId');
		var promoId = req.param('promoId');
		var billingName = req.param('billingName');
		var billingAddress = req.param('billingAddress');
		var billingCity = req.param('billingCity');
		var billingState = req.param('billingState');
		var billingZip = req.param('billingZip');
		var billingCountryCode = req.param('billingCountryCode');
		var shippingName = req.param('shippingName');
		var shippingAddress = req.param('shippingAddress');
		var shippingCity = req.param('shippingCity');
		var shippingState = req.param('shippingState');
		var shippingZip = req.param('shippingZip');
		var shippingCountryCode = req.param('shippingCountryCode');
		var qty = req.param('prodQty');
		var szId = req.param('prodSizeId');
		var flvrId = req.param('prodFlvrId');

		//console.log('Entered paymentTest If');
		//console.log(req.allParams());

		if (tokenId && prodId && billingName && billingAddress && billingCity && billingState 
			&& billingZip && billingCountryCode && shippingName && shippingAddress 
			&& shippingCity && shippingState && shippingZip && shippingCountryCode) {

			//console.log('Entered paymentTest If');

			//get product info
			prodId = prodId.toLowerCase();
			if (!qty) qty = 1; //assume 1 if no qty specified
 				//sails.logger.info('[products] url: ' + req.url);
 				var path = __dirname + '/../../content/products.json';

			fs.readFile(path, function(err, data) {
				if (err) {
					//sails.logger.error('[stories] error: ' + err);
					console.log(err);
					res.serverError();
				}
				

				var products = JSON.parse(data);
				var product = jsonql.findByProdId(products, prodId);


			if (!jsonql.isEmptyObject(product)) {

				//check for promotion
				var promo = null;

				if (promoId) {

					//check for multi-product promo

					promo = checkForMultiProductPromosSync({id: promoId}, prodId);

					if (promo === null) {

						if (product.promotion) {

						for (var i = 0; i < product.promotion.promoCodes.length; i++) {

							if (promoId === product.promotion.promoCodes[i].id) {
								promo = product.promotion.promoCodes[i];
								break;
							}
	 					}
	 				}
 				  }
				}

				var amt = -1;

				if (promo !== null) {

					//check for promo type
					if (promo.type === 'shipping') {
						amt = product.price;
						amt *= qty;
						amt *= (1 + product.taxRate);
						amt += promo.promoPrice != 'undefined' ? promo.promoPrice : product.promotion.promoPrice;
						amt = (amt + 0.30) / (1 - 0.029);

					} else if (promo.type === 'combo') {
						let subtotal = product.price * (1 - promo.combo.price.percentOff / 100);
						amt = subtotal;
						amt *= qty;
						amt *= (1 + product.taxRate);
						amt += promo.combo.shipping.promoPrice;
						//amt = (amt + 0.30) / (1 - 0.029);

						if (subtotal > 0) {
							amt = (amt + 0.30) / (1 - 0.029);
						}

					} else {
						amt = promo.promoPrice != 'undefined' ? promo.promoPrice : product.promotion.promoPrice;
						amt *= qty;
						amt *= (1 + product.taxRate);
						amt += product.shippingPrice;

						if (promo.promoPrice != 'undefined') {
							if (promo.promoPrice > 0) {
								amt = (amt + 0.30) / (1 - 0.029);
							}
						} else {
							if (product.promotion.promoPrice > 0) {
								amt = (amt + 0.30) / (1 - 0.029);
							}
						}
					}


				} else {
						amt = product.price;
						amt *= qty;
						amt *= (1 + product.taxRate);
						amt += product.shippingPrice;
						amt = (amt + 0.30) / (1 - 0.029);
				}

				//console.log(amt);
				
				amt = Math.ceil(amt * 100) / 100; //use math.ceil to ensure proper number format for stripe
				amt *= 100; 
				amt = Math.round(amt);

				if (amt === 0) {
					amt = 50; //$0.50 (stripe's minimum fee)
				}



				//retrieve full token to get customer email
				stripe.tokens.retrieve(tokenId,function(err, token) {

				    //console.log(JSON.stringify(token)); 
				    // Create a Customer:
					stripe.customers.create({
					  email: token.email,
					  source: token.id,
					}).then(function(customer) {
					  // YOUR CODE: Save the customer ID and other info in a database for later.
					 // console.log(JSON.stringify(customer));


					  return stripe.charges.create({
					    amount: amt, //determined by prodId and promoId
					    currency: "usd",
					    customer: customer.id,
					  });
					}).then(function(charge) {
					  // Use and save the charge info.
					  //console.log(JSON.stringify(charge));


					  //load order confirmation template
					  var path = __dirname + '/../../content/email_templates/order_confirmation.html';
					  //use cheerio to modify order confirmation template

					  fs.readFile(path, function(err, data) {
		 					if (err) {
		 						//sails.logger.error('[stories] error: ' + err);
		 						console.log(err);
		 						res.serverError();
		 					}

		 					//load template into cheerio and update it with customer, product, & promo info
		 					var $ = cheerio.load(data);


		 					//get customer, product, & promo info
		 					var name = billingName;
		 					var nameArry = name.split(' ');
		 					var firstName = nameArry[0];
		 					var lastName = nameArry.length > 1 ? nameArry[1] : '';
		 					var shipNameArry = shippingName.split(' ');
		 					var shipFirstName = shipNameArry[0];
		 					var shipLastName = shipNameArry.length > 1 ? shipNameArry[1] : '';
		 					var shippingAddressFull = shippingName + '<br>' + shippingAddress + '<br>' 
		 										  + shippingCity + ', ' + shippingState;

		 					if (shippingCountryCode != 'US') shippingAddressFull += ', ' + shippingCountryCode;
		 					shippingAddressFull += '<br>' + shippingZip;

		 					var billingAddressFull = billingName + '<br>' + billingAddress + '<br>' 
		 										  + billingCity + ', ' + billingState;

		 					if (billingCountryCode != 'US') billingAddressFull += ', ' + billingCountryCode;
		 					billingAddressFull += '<br>' + billingZip;

		 					//var orderNumber = helper.createOrderNum();
		 					var prodName = product.fullName;
		 					var size = '';
		 					var regPrice = -1;
		 					var img = product.img;

		 					if (szId) {
		 						//loop through sizes to find correct one (LATER#)
		 						//update the size and the price
		 					} else {
		 						//default to the first size option
		 						size = product.sizes[0].size;
		 						regPrice = product.sizes[0].price;
		 					}

		 					var price = regPrice;
		 					var shipping = product.shippingPrice;

		 					if (promo) {
		 						if (promo.type === 'shipping') {
		 							shipping = product.promotion.promoPrice;
		 						} else if (promo.type === 'combo') {
		 							price = regPrice * (1 - promo.combo.price.percentOff / 100);
		 							shipping = promo.combo.shipping.promoPrice;
		 						} else {
		 							price = promo.promoPrice != 'undefined' ? promo.promoPrice : product.promotion.promoPrice;
		 						}
		 						
		 						if (promo.pImg) img = promo.pImg;
		 					}

		 					var subtotal = price * qty;
		 					
		 					var tax = subtotal * product.taxRate;
		 					var total = charge.amount / 100;
		 					var fee = (0.029 * total + 0.30);

		 					if (price === 0) {
		 						fee = 0;
		 					}

		 					var cardBrand = charge.source.brand;
		 					var cardDigits = charge.source.last4;


		 					//update template
		 					$('#oc-first-name').text(firstName);
		 					
		 					$('#oc-prod-img').attr('src', 'https://alwayslivon.com' + img);
		 					$('#oc-prod-name').text(prodName);
		 					$('#oc-prod-size').text(size);
		 					$('#oc-prod-qty').text(qty);

		 					$('#oc-prod-price').text('$' + regPrice);
		 					

		 					if (promo) {
		 						if (promo.type === 'shipping') {
		 							$('.oc-promo-container').first().attr('style', 'display: inline-block');
		 							$('#oc-promo-code').text(promo.code);
		 						} else if (promo.type === 'combo') {
		 							$('.oc-promo-container').attr('style', 'display: inline-block');
			 						$('#oc-promo-price').text('$' + Math.ceil(price * 100) / 100 );
			 						$('#oc-promo-code').text(promo.code);
			 						$('#oc-prod-price').attr('style', 'text-decoration: line-through');
		 						} else {
		 							$('.oc-promo-container').attr('style', 'display: inline-block');
			 						$('#oc-promo-price').text('$' + price);
			 						$('#oc-promo-code').text(promo.code);
			 						$('#oc-prod-price').attr('style', 'text-decoration: line-through');
		 						}
		 						
		 					}

		 					$('#oc-shipping-address').html(shippingAddressFull);
		 					$('#oc-billing-address').html(billingAddressFull);
		 					$('#oc-prod-subtotal').text('$' + Math.ceil(subtotal * 100) / 100);
		 					$('#oc-prod-shipping').text('$' + shipping);
		 					$('#oc-prod-tax').text('$' + Math.ceil((tax + fee) * 100) / 100);
		 					$('#oc-prod-total').text('$' + total);

		 					$('#oc-card-brand').text(cardBrand + ' ');
		 					$('#oc-card-4-digits').text(cardDigits);


		 					//add order to db
		 					db.getPaymentTypeByName('stripe', function(err, paymentType) {


		 						var orderObj = {};

		 						//console.log(shippingState);
		 						//console.log(billingState);

			 					orderObj.orderDatetime = new Date().toISOString(); //* check format
			 					orderObj.orderNum = null;
			 					orderObj.shipFirstName = shipFirstName;
			 					orderObj.shipLastName = shipLastName;
			 					orderObj.email = token.email;
			 					orderObj.company = null;
			 					orderObj.productId = parseInt(product.prodId);
			 					orderObj.qty = parseInt(qty);
			 					orderObj.isPersonalized = 'N'; //if woman, it should be personalized
			 					orderObj.promoTypeId = null; //query promotypes
			 					orderObj.promoCode = null;
			 					orderObj.promoPrice = null;
			 					orderObj.promoShipping = null;
			 					orderObj.promoDesc = null;

			 					

			 					orderObj.shipAddress = shippingAddress;
			 					orderObj.shipCity = shippingCity;
			 					orderObj.shipState = shippingState;
			 					orderObj.shipZip = shippingZip;
			 					orderObj.billFirstName = firstName;
			 					orderObj.billLastName = lastName;
			 					orderObj.billAddress = billingAddress;
			 					orderObj.billCity = billingCity;
			 					orderObj.billState = billingState;
			 					orderObj.billZip = billingZip;
			 					orderObj.paymentTypeId = paymentType.Id; //query paymenttypes
			 					orderObj.subtotal = Math.ceil(subtotal * 100) / 100;
			 					orderObj.shipping = shipping;
			 					orderObj.taxesFees = Math.ceil((tax + fee) * 100) / 100;
			 					orderObj.total = total;
			 					orderObj.isPaymentCollected = 'Y';
			 					orderObj.cardCharged = cardBrand + ' ' + cardDigits;
			 					orderObj.comment = null;
			 					orderObj.isSampleOrder = 'N';
			 					orderObj.userId = 1; //1 is always reserved for Liv On. //In the future, the userId could be expanded to actually site user/customer accounts, but that wouldn't affect this order management process

			 					 var customerObj = {

												email: orderObj.email,
												firstName: orderObj.shipFirstName,
												lastName: orderObj.shipLastName,
												company: orderObj.company,
												userId: orderObj.userId
											};

			 					if (promo) {

			 						db.getPromoTypeByName(promo.type, function(err, promoType) {

			 							if (err) {
			 								console.log(err);
			 								return;
			 							}

			 							orderObj.promoTypeId = promoType.Id; //query promotypes
					 					orderObj.promoCode = promo.code;

					 					if (promo.type === 'shipping') {
				 							orderObj.promoShipping = product.promotion.promoPrice;
				 						} else if (promo.type === 'combo') {
				 							orderObj.promoPrice = regPrice * (1 - promo.combo.price.percentOff / 100);
				 							orderObj.promoShipping = promo.combo.shipping.promoPrice;
				 						} else {
				 							orderObj.promoPrice = promo.promoPrice != 'undefined' ? promo.promoPrice : product.promotion.promoPrice;
				 						}
					 					
					 					orderObj.promoDesc = promo.desc;

					 					db.addNewOrder(orderObj, function(err, resObj) {
					 						if (err) {
					 							console.log(err);
					 							return;
					 						}

					 						if (resObj.result === 'fail') {
					 							console.log('Add new order failed');
					 						}

					 						//get the order num from the resObj and send email
					 						$('#oc-order-num').text(resObj.orderNum);

					 						helper.mail(sails.config.lv.DisplayName + ' <' + sails.config.lv.Email + '>', 
											  	token.email,
											  	sails.config.lv.Email2 + ',' + sails.config.lv.Email,
											  	'',
											  	'Order Confirmation',
											  	$.html(),
											  	function(result) {
											  		//redirect to order confirmation
											  		if (result === 'success') {

											  			res.view('checkout/order-confirmation', { firstName: firstName, email: token.email}); //send the customer name & email to the view
											  		} else {
											  			res.serverError();
											  		}

													
											  	});					 						




											db.addGetCustomer(customerObj, function(err, customer) {

												if (err) {
													throw err;
												}

												var o = {
													customerId: customer.Id,
													email: orderObj.email,
													firstName: orderObj.shipFirstName,
													lastName: orderObj.shipLastName,
													orderId: resObj.orderNum,
													orderTotal: orderObj.total,
													orderDatetime: orderObj.orderDatetime,
													orderLines: [{ 
															id: orderObj.productId,
															product_id: orderObj.productId,
															product_variant_id: orderObj.productId,
															quantity: orderObj.qty,
															price: price
													}],
													options: {storeId: '01'}

												}


												//add order to mailchimp store.
												//don't throw error. Order was successfully added.
												//send warning if error so that client knows order
												//didn't go through on mailchimp

												//consider running create as an asyncronous task
												//that doesn't affect ui performance
												//(at the same time, I do want to know
												//that mailchimp was properly updated)

												mef.mailchimp.orders.create(o, function(err, resp) {

													if (err) {
														console.log(JSON.stringify(err));
													}

													console.log(JSON.stringify(resp));


												});




											});


					 					});

				


			 						});
			 					} else {
			 						db.addNewOrder(orderObj, function(err, resObj) {
					 						if (err) {
					 							console.log(err);
					 							return;
					 						}

					 						if (resObj.result === 'fail') {
					 							console.log('Add new order failed');
					 						}

					 						//get the order num from the resObj and send email
					 						$('#oc-order-num').text(resObj.orderNum);

					 						helper.mail(sails.config.lv.DisplayName + ' <' + sails.config.lv.Email + '>', 
											  	token.email,
											  	sails.config.lv.Email2 + ',' + sails.config.lv.Email,
											  	'',
											  	'Order Confirmation',
											  	$.html(),
											  	function(result) {
											  		//redirect to order confirmation
											  		if (result === 'success') {

											  			res.view('checkout/order-confirmation', { firstName: firstName, email: token.email}); //send the customer name & email to the view
											  		} else {
											  			res.serverError();
											  		}

													
											  	});
			 							//add the order to mailchimp
					 					//try doing this asyncronously

											db.addGetCustomer(customerObj, function(err, customer) {

												if (err) {
													throw err;
												}

												var o = {
													customerId: customer.Id,
													email: orderObj.email,
													firstName: orderObj.shipFirstName,
													lastName: orderObj.shipLastName,
													orderId: resObj.orderNum,
													orderTotal: orderObj.total,
													orderDatetime: orderObj.orderDatetime,
													orderLines: [{ 
															id: orderObj.productId,
															product_id: orderObj.productId,
															product_variant_id: orderObj.productId,
															quantity: orderObj.qty,
															price: price
													}],
													options: {storeId: '01'}

												}


												//add order to mailchimp store.
												//don't throw error. Order was successfully added.
												//send warning if error so that client knows order
												//didn't go through on mailchimp

												//consider running create as an asyncronous task
												//that doesn't affect ui performance
												//(at the same time, I do want to know
												//that mailchimp was properly updated)

												mef.mailchimp.orders.create(o, function(err, resp) {

													if (err) {
														console.log(JSON.stringify(err));
													}

													console.log(JSON.stringify(resp));


												});




											});


					 					});
			 					}

		 					});			

		 				});

					  
					});
				  }
				);
			  } else {
			  		res.notFound();
			  }
			});
		 
		}

		//get the product 
		//check if there is a valid promoId
		//apply the promotion if so
		//charge the customer

		

		// YOUR CODE (LATER): When it's time to charge the customer again, retrieve the customer ID.
		/*stripe.charges.create({
		  amount: 1500, // $15.00 this time
		  currency: "usd",
		  customer: customerId,
		});*/

		//send order confirmation email (1 to customer, 1 to customer.service@livon.thatnow.com)

		

	},
	paymentInfo: function(req, res) { //make sure to secure this
		//fill in details

		if (req.wantsJSON) {
			//sails.logger.info('[achievements - find] url: ' + req.url);
		  	//console.log('find');
	 		//console.log(req.param('name'));
	 		var prodId = req.param('product'); //integer

	 		if (prodId) {
	 				var path = __dirname + '/../../content/products.json';

	 				fs.readFile(path, function(err, data) {
	 					if (err) {
	 						//sails.logger.error('[achievements - find] error: ' + err);
	 						//console.log(err);
	 						res.serverError();
	 					}
	 					//need to query json object for id that matches achvName
	 					//if no match, 404 error
	 					var products = JSON.parse(data);
	 					var product = jsonql.findByProdId(products, prodId);

	 					 //this should have all info needed
	 					//for an order confirmation (NOT TRUE). it must include all the info 
	 					//needed to complete the transaction and forward the prod id & 
	 					//promo information to the orderConfirmation action
	 					//the product id, the product name, the product description 

	 					//check the url query for promo id and promo=true

	 					//does it make security sense to check if the promo was applied on the
	 					//client side? I don't think so.

	 				if (!jsonql.isEmptyObject(product)) {

	 					//check for promotion
						var promo = null;

						if (req.query.promo && req.query.promoId) {

							//check for multi-product promo 
							promo = checkForMultiProductPromosSync({id: req.query.promoId}, prodId);

							if (promo === null) {

								if (product.promotion) {
									for (var i = 0; i < product.promotion.promoCodes.length; i++) {

										if (req.query.promoId === product.promotion.promoCodes[i].id) {
											promo = product.promotion.promoCodes[i];
											break;
										}
		 							}
								}
							}



						}

						var amt = product.price;
						var shipping = product.shippingPrice;

						if (promo != null) {
							if (promo.type === 'shipping') {
								amt = product.price;
							} else if (promo.type === 'combo') {
								amt = (1 - promo.combo.price.percentOff / 100) * product.sizes[0].price;
								shipping = promo.combo.shipping.promoPrice;
							} else {
								amt = promo.promoPrice != 'undefined' ? promo.promoPrice : product.promotion.promoPrice;
							}
						} 

						//var amt = promo != null ? product.promotion.promoPrice : product.price;
						//amt *= (1 + product.taxRate);
						//amt += product.shippingPrice;
						//amt = Math.ceil(amt * 100) / 100; //use math.round to ensure proper number format for stripe

						var paymentInfo = { prodName: product.fullName,
											prodSize: product.sizes[0].size, //refactor later
											subtotal: amt,
											taxRate: product.taxRate,
											shipping: shipping,
											promoCode: '-1',
											promoType: 'na',
											promoDesc: 'na',
											promoPrice:  -1,
											paypalId: sails.config.lv.PaypalId,
											paypalUrl: sails.config.lv.PaypalUrl
										   }

						if (promo != null) {
							paymentInfo.promoCode = promo.code;
							paymentInfo.promoType = promo.type;
							paymentInfo.promoPrice = promo.promoPrice != 'undefined' ? promo.promoPrice : product.promotion.promoPrice;
							paymentInfo.promoDesc = promo.desc;

							if (promo.type === 'combo') {
								//paymentInfo.promoPrice = (1 - promo.combo.price.percentOff / 100) * product.sizes[0].price;
							}

						}

						//console.log(JSON.stringify(paymentInfo))


						//return totalAmount
	 					res.json(paymentInfo);


	 				} else {
	 					res.json({error: "not found"});
	 				}		

	 			});
	 		} else {
	 			res.json({error: "no prod Id"});//sails.logger.error('[achievements - find] error: no story id provided; url: ' + req.url);
	 		}
	 	  } else {
	 	  		res.json({error: "json requests only"});//sails.logger.error('[achievements - find: json required] url: ' + req.url + '; msg: html request to json endpoint');
	 	  }
		},

		payPalInfo: function(req, res) { //make sure to secure this

			var payPalInfo = {
				paypalId: sails.config.lv.PaypalId,
				paypalUrl: sails.config.lv.PaypalUrl
			};

			res.json(payPalInfo);
			

		},

		oneExp: function(req, res) {


			//get the product by id and return data to view
		var prodId = req.param('product');

		//console.log(JSON.stringify(req.params));
			//console.log(req.query);

 		if (prodId) {
				prodId = prodId.toLowerCase();
 				//sails.logger.info('[products] url: ' + req.url);
 				var path = __dirname + '/../../content/products.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						//sails.logger.error('[stories] error: ' + err);
 						console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
					//console.log("test");
					//console.log(data.toString());
 					var products = JSON.parse(data);
 					var product = jsonql.findById(products, prodId);
 					product.title = 'Liv On ' +  product.name + ' | ' + product.description;

 					//console.log(JSON.stringify(story));

 					//check if story is available - if so 

 					var recExists = !jsonql.isEmptyObject(product);

 					if (recExists) {

 						//check for promo in url query
 						if (req.query.promo && req.query.promoId) {
 							//check to see if promo is avalaible
 							//if so, add info to product (product.validUrlPromo)
 							//update view to reflect that promo has been applied
 							if (product.promotion) {

 								var thisPromo = null;

 								for (var i = 0; i < product.promotion.promoCodes.length; i++) {
 									if (req.query.promoId === product.promotion.promoCodes[i].id) {
 										thisPromo = product.promotion.promoCodes[i];
 										break;
 									}
 								}

 								if (thisPromo) {
 									if (thisPromo.valid === true) {

 										var price = thisPromo.promoPrice != 'undefined' ? thisPromo.promoPrice : product.promotion.promoPrice;

 										product.validUrlPromo = {id: thisPromo.id,
 																 code: thisPromo.code, 
 															  	 price: price,
 															  	 img: thisPromo.pImg,
 															  	 type: thisPromo.type};

 									}

 									if (thisPromo.combo) {
 										product.validUrlPromo.combo = thisPromo.combo;
 									}

 									if (thisPromo.qty) {
 										product.validUrlPromo.qty = thisPromo.qty;
 									}

 									if (thisPromo.desc) {
 										product.validUrlPromo.desc = thisPromo.desc;
 									}
 								} else {
 									product.validUrlPromo = null;
 								}
 							}
 						}
 						//console.log(JSON.stringify(product.validUrlPromo));
 						//product.layout = 'layout-sales-letters';
 						//req.session.liv = "DON'T OVERWRITE";
 						res.view('checkout/one-exp', product);
 					} else {
 						//sails.logger.error('[products] error: no record found; url: ' + req.url);
 						res.notFound();
 					}

 				});
 		} else {
 			//sails.logger.error('[products] error: no product id provided; url: ' + req.url);
 			res.notFound();
 		}


	},

	submitPaymentX: function(req, res) {

		//to prevent attackers from changing the price, total, etc.,
		//I can just call the mef api again for the runId

		var tokenId = req.param('stripeToken');
		var prodName = req.param('prodName');
		var prodFlavor = req.param('prodFlavor');
		var prodDesc = req.param('prodDesc');
		var prodPrice = req.param('prodPrice');
		var prodSubtotal = req.param('prodSubtotal');
		var prodShipping = req.param('prodShipping');
		var prodTaxesFees = req.param('prodTaxesFees');
		var prodSize = req.param('prodSize');
		var prodImg = req.param('prodImg');
		var prodNutritionImg = req.param('prodNutritionImg');
		var promoName = req.param('promoName');
		var prodQty = req.param('prodQty');
		var billingName = req.param('billingName');
		var billingAddress = req.param('billingAddress');
		var billingCity = req.param('billingCity');
		var billingState = req.param('billingState');
		var billingZip = req.param('billingZip');
		var billingCountryCode = req.param('billingCountryCode');
		var shippingName = req.param('shippingName');
		var shippingAddress = req.param('shippingAddress');
		var shippingCity = req.param('shippingCity');
		var shippingState = req.param('shippingState');
		var shippingZip = req.param('shippingZip');
		var shippingCountryCode = req.param('shippingCountryCode');
		var test;

		//console.log(JSON.stringify(req.allParams()));

		if (tokenId && prodName && prodFlavor && prodDesc && prodPrice && prodSubtotal
		&& prodShipping && prodTaxesFees && prodSize && prodImg 
		&& prodQty && billingName && billingAddress && billingCity && billingState 
		&& billingZip && billingCountryCode && shippingName && shippingAddress 
			&& shippingCity && shippingState && shippingZip && shippingCountryCode) {

			//console.log('submitPaymentX'); 

			function getNum(moneyString) {
				const rgx = /[$€£]+/g;
				var num = parseFloat(moneyString.replace(rgx, ''));
				if (isNaN(num)) {
					res.serverError();
				} else {
					return num;
				}
			}

			if (prodName.toLowerCase().indexOf('liv on') === -1) {
				prodName = 'Liv On ' + prodName;
			}

			prodPrice = getNum(prodPrice);
			prodSubtotal = getNum(prodSubtotal);
			prodShipping = getNum(prodShipping);
			prodTaxesFees = getNum(prodTaxesFees);
			prodQty = getNum(prodQty);


			var amt = prodSubtotal + prodShipping + prodTaxesFees;




			//retrieve full token to get customer email
			stripe.tokens.retrieve(tokenId,function(err, token) {

				    //console.log(JSON.stringify(token)); 
				    // Create a Customer:
					stripe.customers.create({
					  email: token.email,
					  source: token.id,
					}).then(function(customer) {
					  // YOUR CODE: Save the customer ID and other info in a database for later.
					 // console.log(JSON.stringify(customer));


					  return stripe.charges.create({
					    amount: amt * 100, 
					    currency: "usd", //this will have to be update as Liv On expands
					    customer: customer.id,
					  });
					}).then(function(charge) {
					  // Use and save the charge info.
					  //console.log(JSON.stringify(charge));


					  //load order confirmation template
					  var path = __dirname + '/../../content/email_templates/order_confirmation.html';
					  //use cheerio to modify order confirmation template

					  fs.readFile(path, function(err, data) {
		 					if (err) {
		 						console.log(err);
		 						res.serverError();
		 					}

		 					//load template into cheerio and update it with customer, product, & promo info
		 					var $ = cheerio.load(data);


		 					//get customer, product, & promo info
		 					var name = billingName;
		 					var nameArry = name.split(' ');
		 					var firstName = nameArry[0];
		 					var lastName = nameArry.length > 1 ? nameArry[1] : '';
		 					var shipNameArry = shippingName.split(' ');
		 					var shipFirstName = shipNameArry[0];
		 					var shipLastName = shipNameArry.length > 1 ? shipNameArry[1] : '';
		 					var shippingAddressFull = shippingName + '<br>' + shippingAddress + '<br>' 
		 										  + shippingCity + ', ' + shippingState;

		 					if (shippingCountryCode != 'US') shippingAddressFull += ', ' + shippingCountryCode;
		 					shippingAddressFull += '<br>' + shippingZip;

		 					var billingAddressFull = billingName + '<br>' + billingAddress + '<br>' 
		 										  + billingCity + ', ' + billingState;

		 					if (billingCountryCode != 'US') billingAddressFull += ', ' + billingCountryCode;
		 					billingAddressFull += '<br>' + billingZip;

		 					//var orderNumber = helper.createOrderNum();
		 					var size = prodSize;
		 					var img = prodImg;

		 					var price = prodPrice;
		 					var qty = prodQty;
		 					//console.log(prodShipping);
		 					var shipping = prodShipping;



		 					var subtotal = prodSubtotal; //need to account for qty
		 					
		 					//var tax = subtotal * product.taxRate;
		 					var total = charge.amount / 100;
		 					//var fee = (0.029 * total + 0.30);

		 					/*if (price === 0) {
		 						fee = 0;
		 					}*/

		 					var cardBrand = charge.source.brand;
		 					var cardDigits = charge.source.last4;


		 					//update template
		 					$('#oc-first-name').text(firstName);
		 					
		 					$('#oc-prod-img').attr('src', img); //img path must be fully-qualified
		 					$('#oc-prod-name').text(prodName + ' ' + prodFlavor);
		 					$('#oc-prod-size').text(size);
		 					$('#oc-prod-qty').text(qty);

		 					$('#oc-prod-price').text('$' + price);
		 					

		 					if (promoName) {

		 							$('.oc-promo-container').attr('style', 'display: inline-block');
			 						$('#oc-promo-price').text('$' + subtotal);
			 						$('#oc-promo-code').text(promoName);
			 						$('#oc-prod-price').attr('style', 'text-decoration: line-through');
		 						
		 					}

		 					$('#oc-shipping-address').html(shippingAddressFull);
		 					$('#oc-billing-address').html(billingAddressFull);
		 					$('#oc-prod-subtotal').text('$' + subtotal);
		 					$('#oc-prod-shipping').text('$' + shipping);
		 					$('#oc-prod-tax').text('$' + prodTaxesFees);
		 					$('#oc-prod-total').text('$' + total);

		 					$('#oc-card-brand').text(cardBrand + ' ');
		 					$('#oc-card-4-digits').text(cardDigits);


		 					//add order to db
		 					db.getPaymentTypeByName('stripe', function(err, paymentType) {


		 						var orderObj = {};

		 						//console.log(shippingState);
		 						//console.log(billingState);

			 					orderObj.orderDatetime = new Date().toISOString(); //* check format
			 					orderObj.orderNum = null;
			 					orderObj.shipFirstName = shipFirstName;
			 					orderObj.shipLastName = shipLastName;
			 					orderObj.email = token.email;
			 					orderObj.company = null;
			 					orderObj.prodId = -1; // parseInt(product.prodId);
			 					orderObj.prodName = prodName;
			 					orderObj.prodFlavor = prodFlavor;
			 					orderObj.prodPrice = prodPrice;
			 					orderObj.prodSize = prodSize;
			 					orderObj.qty = qty;
			 					orderObj.isPersonalized = 'N'; //if woman, it should be personalized
			 					orderObj.promoTypeId = null; //query promotypes
			 					orderObj.promoCode = null;
			 					orderObj.promoPrice = promoName !== undefined ? subtotal : null;
			 					orderObj.promoShipping = null;
			 					orderObj.promoDesc = promoName || null;

			 					

			 					orderObj.shipAddress = shippingAddress;
			 					orderObj.shipCity = shippingCity;
			 					orderObj.shipState = shippingState;
			 					orderObj.shipZip = shippingZip;
			 					orderObj.billFirstName = firstName;
			 					orderObj.billLastName = lastName;
			 					orderObj.billAddress = billingAddress;
			 					orderObj.billCity = billingCity;
			 					orderObj.billState = billingState;
			 					orderObj.billZip = billingZip;
			 					orderObj.paymentTypeId = paymentType.Id; //query paymenttypes
			 					orderObj.subtotal = subtotal;
			 					orderObj.shipping = shipping;
			 					orderObj.taxesFees = prodTaxesFees;
			 					orderObj.total = total;
			 					orderObj.isPaymentCollected = 'Y';
			 					orderObj.cardCharged = cardBrand + ' ' + cardDigits;
			 					orderObj.comment = prodDesc || null;
			 					orderObj.isSampleOrder = 'N';
			 					orderObj.userId = 1; //1 is always reserved for Liv On. //In the future, the userId could be expanded to actually site user/customer accounts, but that wouldn't affect this order management process

			 					
	
			 					orderObj.prodImg = prodImg || null;
			 					orderObj.prodNutritionImg = prodNutritionImg || null;


			 					//console.log('Made it!');

					 					db.addNewExpOrder(orderObj, function(err, resObj) {
					 						if (err) {
					 							console.log(err);
					 							return;
					 						}

					 						if (resObj.result === 'fail') {
					 							console.log('Add new order failed');
					 						}


					 						//get the order num from the resObj and send email
					 						$('#oc-order-num').text(resObj.orderNum);

					 						helper.mail(sails.config.lv.DisplayName + ' <' + sails.config.lv.Email + '>', 
											  	token.email,
											  	sails.config.lv.Email2 + ',' + sails.config.lv.Email,
											  	'',
											  	'Order Confirmation',
											  	$.html(),
											  	function(result) {
											  		//redirect to order confirmation
											  		if (result === 'success') {

											  			res.view('checkout/order-confirmation', { firstName: firstName, email: token.email}); //send the customer name & email to the view
											  		} else {
											  			res.serverError();
											  		}

													
											  	});
					 					}); 


			 						}); // get payment type
			 					}); //read file

			 				}); //charge

			 			});	//token open
						



	}

	//res.view('checkout/order-confirmation', { firstName: 'test', email: 'test'}); //send the customer name & email to the view

}

}