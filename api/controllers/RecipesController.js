/* Recipes Controller */
var fs = require('fs');
var helper = require(__dirname + '/../../helpers/helper');
var https = require('https');



module.exports = {
	index: function(req, res) {
		//sails.logger.info('[html] url: ' + req.url);

		var id = req.param('id');

		if (id) {

			fs.stat(__dirname +  '/../../views/recipes/' + id + '.ejs', function(err, stats) {
				if (err) {
					res.view('recipes/index');
				} else {
					res.view('recipes/' + id);
				}
			});
			
		} else {
			res.view('recipes/index');
		}

	},
	signup: function(req, res) {

		if (req.param('email') && req.param('firstName') 
			&& req.param('list')) {

				//define data params
				var email = req.param('email');
				var firstName = req.param('firstName');
				var list = req.param('list');

				//look up list to make sure it's valid. If not, default to Liv On Community


				//ignoring subscribers' location information for now
				var query = JSON.stringify({
							 "email_address" : email,
							 "status" : "subscribed",
							 "status_if_new" : "subscribed",
							 "merge_fields" : { "FNAME" : firstName }
							});


					//console.log("host : " + sails.config.jh.MailChimpHost);
					//console.log("path : " + sails.config.jh.MailChimpSubscribePath);
					//console.log("api key : " + sails.config.jh.MailChimpAPIKey);

					var hash = helper.md5(email.toLowerCase());

					var path = sails.config.lv['MailChimpSubscribePath' + list];
					path = path ? path : sails.config.lv.MailChimpSubscribePath;

				var options = {
					host: sails.config.lv.MailChimpHost,
					path: path + '/' + hash,
					method: 'PUT',
					headers: {
						"Authorization" : sails.config.lv.MailChimpAPIKey,
						"Content-Type" : "application/json",
						"Content-Length" : query.length
					}
				}

				var hreq = https.request(options, function(hres) {
					//console.log('Status Code: ' + hres.statusCode);
					//console.log('Headers: ' + JSON.stringify(hres.headers));
					//sails.logger.info('[subscribe] status-code: ' + hres.statusCode);
					

					hres.setEncoding('utf8');

					hres.on('data', function(chunk) {
						console.log('\n\nCHUNK');
						console.log(chunk);
					});

					hres.on('end', function(end) {
						console.log('\n\nEND');
						res.send("success");
					});

					hres.on('error', function(err) {
						//sails.logger.error('[subscribe] error: ' + err + '; url: ' + req.url);
						//sails.logger.error('[subscribe] headers: ' + hres.headers);
						//console.log('Error: ' + err.message);
					});
				});

				hreq.write(query);
				hreq.end();

		}
		else {
			var err = 'Invalid subscription arguments';
			//sails.logger.error('[subscribe] error: ' + err + '; url: ' + req.url);
			res.serverError(err);
		}



	}
}