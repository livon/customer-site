/* Sales letter Controller */



module.exports = {
	index: function(req, res) {

		var ltrId = req.param('name');

		if (ltrId) {
			var vw = 'letters/' + ltrId;
			var t = 'Liv On | ' + ltrId.replace(/-/g,' ');
			res.view(vw, { layout: 'layout-sales-letters', title: t });
		} else {
			res.notFound();
		}
	
		

	}
}