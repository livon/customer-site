/********* Mobile Design Functionality **********/

$(document).ready(function() {

	$('#liv-hamburger').click(function() {

		//rotate hamburger (toggle)

		$(this).toggleClass('open');

		//open mobile menu (toggle)
		$('#liv-nav-mb').toggleClass('open');
	});


	$('.liv-prod-details-hdr').each(function() {
		$(this).click(function() {
			toggleSection($(this));
		});
	});


	function toggleSection(hdrOrArrw) {

		var container = $(hdrOrArrw).closest('.liv-prod-details-container-mb');

		container.toggleClass('expanded');
		container.find('.liv-arrow').toggleClass('up');
	}
});