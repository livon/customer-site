/****** Checkout Handler (Stripe Integration) *********/
var handlerX = StripeCheckout.configure({
  key: 'pk_test_qSlhwvT2OX1HXZlio1ufFpM6', //pk_live_kq2Ts4DqXugNGvVi7qVYbUcj | pk_test_qSlhwvT2OX1HXZlio1ufFpM6
  image: '/images/liv_on_checkout_logo.png', 
  locale: 'auto',
  token: function(token, args) {
  	//console.log('token called');
  	//console.log(JSON.stringify(args));
    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.
    $('#liv-img-processing').removeClass('liv-hidden');

    //var prodId = $('#liv-product-container').data('prodId');
    //var promoId = $('#liv-product-container').data('promoId');
    var prodName = $('#liv-x-prod-name').text();
    var flavorInput = $('input[name="flavor"]:checked');
    var prodFlavor = flavorInput.parent().find('.liv-flvr-lbl').first().text(); 
    var prodPrice = $('#liv-price').text();
    var prodSubtotal = $('#liv-subtotal').text();
    var prodShipping = $('#liv-shipping').text();
    var prodTaxesFees = $('#liv-tax').text();
    var prodSize = $('#liv-size').text();
    var prodImg = $('#liv-img-prod-details').attr('src');
    var prodNutritionImg = $('.liv-img-extra')[0].src;
    var liDesc = $('#liv-prod-desc-list li');
    var prodDesc = '';

    $.each(liDesc, function(i, li) {

    	prodDesc += $(li).text();

    	if (i < liDesc.length) {
    		prodDesc += ';';
    	}
    	
    });
    var qty = $('#liv-qty-x').val();
    var promoName = $('#liv-promo-name').text();
    var promoPrice = $('#liv-promo-price').text();
    var csrf = $('#liv-product-container').data('csrf');
    //console.log(csrf);



    var form = $('<form action="/checkout/submitPaymentX" method="post"></form>');
    form.append('<input type="hidden" name="stripeToken" value="' + token.id + '"/>');
    form.append('<input type="hidden" name="prodName" value="' + prodName + '"/>');
    form.append('<input type="hidden" name="prodFlavor" value="' + prodFlavor + '"/>');
    form.append('<input type="hidden" name="prodDesc" value="' + prodDesc + '"/>'); 
    form.append('<input type="hidden" name="prodPrice" value="' + prodPrice + '"/>');
    form.append('<input type="hidden" name="prodSubtotal" value="' + prodSubtotal + '"/>');
    form.append('<input type="hidden" name="prodShipping" value="' + prodShipping + '"/>');
    form.append('<input type="hidden" name="prodTaxesFees" value="' + prodTaxesFees + '"/>');
    form.append('<input type="hidden" name="prodSize" value="' + prodSize + '"/>');
    form.append('<input type="hidden" name="prodImg" value="' + prodImg + '"/>');
    form.append('<input type="hidden" name="prodNutritionImg" value="' + prodNutritionImg + '"/>');
    form.append('<input type="hidden" name="prodQty" value="' + qty + '"/>');
    form.append('<input type="hidden" name="promoName" value="' + promoName + '"/>'); 
    form.append('<input type="hidden" name="billingName" value="' + args.billing_name + '"/>');
    form.append('<input type="hidden" name="billingAddress" value="' + args.billing_address_line1 + '"/>');
    form.append('<input type="hidden" name="billingCity" value="' + args.billing_address_city + '"/>');
    form.append('<input type="hidden" name="billingState" value="' + args.billing_address_state + '"/>');
    form.append('<input type="hidden" name="billingZip" value="' + args.billing_address_zip + '"/>');
    form.append('<input type="hidden" name="billingCountryCode" value="' + args.billing_address_country_code + '"/>');
    form.append('<input type="hidden" name="shippingName" value="' + args.shipping_name + '"/>');
    form.append('<input type="hidden" name="shippingAddress" value="' + args.shipping_address_line1 + '"/>');
    form.append('<input type="hidden" name="shippingCity" value="' + args.shipping_address_city + '"/>');
    form.append('<input type="hidden" name="shippingState" value="' + args.shipping_address_state + '"/>');
    form.append('<input type="hidden" name="shippingZip" value="' + args.shipping_address_zip + '"/>');
    form.append('<input type="hidden" name="shippingCountryCode" value="' + args.shipping_address_country_code + '"/>');
    form.append('<input type="hidden" name="_csrf" value="' + csrf + '"/>');

    console.log(form.serialize());


    $('body').append(form);

    form.submit();

  }
});

$(document).ready(function() {






	//Stripe

	$('#liv-btn-checkout-exp').click(function(e) {
		e.preventDefault();

		console.log('test');

		stripeItX();
	});


// Close Stripe Checkout on page navigation:
window.addEventListener('popstate', function() {
  handlerX.close();
});

	
	//PayPal

$('#paypal-submit-exp').click(function(e) {
		e.preventDefault();

		paypalItX();

		
	});


	function stripeItX() {
		var prodName = $('#liv-x-prod-name').text();
		var prodPrice = $('#liv-total').text();
		prodPrice = prodPrice.substring(1, prodPrice.length);
		prodPrice = parseFloat(prodPrice) * 100;

		 handlerX.open({
    		name: 'Liv On',
    		description: prodName, //name of product
    		amount: prodPrice, //price
    		billingAddress: true,
    		shippingAddress: true,
    		zipCode: true
  		});
	}

	function paypalItX() {
		var prodName = $('#liv-x-prod-name').text();
	    var flavorInput = $('input[name="flavor"]:checked');
	    var prodFlavor = flavorInput.parent().find('.liv-flvr-lbl').first().text(); 
	    var prodPrice = getNum($('#liv-price').text());
	    var prodSubtotal = getNum($('#liv-subtotal').text());
	    var prodShipping = getNum($('#liv-shipping').text());
	    var prodTaxesFees = getNum($('#liv-tax').text());
	    var prodSize = $('#liv-size').text();
	    var prodImg = $('#liv-img-prod-details').attr('src');
	    var prodNutritionImg = $('.liv-img-extra')[0].src;
	    var liDesc = $('#liv-prod-desc-list li');
	    var prodDesc = '';

	    $.each(liDesc, function(i, li) {

	    	prodDesc += $(li).text();

	    	if (i < liDesc.length) {
	    		prodDesc += ';';
	    	}
	    	
	    });

	    if (prodName.toLowerCase().indexOf('liv on') === -1) {
	    	prodName = 'Liv On ' + prodName;
	    }


	    var itemName = prodName + ' - ' + prodFlavor + ' - ' + prodSize;
	    var qty = $('#liv-qty-x').val();
	    if (qty < 1) qty = 1;
	    var promoName = $('#liv-promo-name').text();
	    var promoPrice = $('#liv-promo-price').text();

	    if (promoName) {
			itemName += ' - promo: ' + promoName;
		}

		itemName += ' | Qty: ' + qty;
		itemName += ' | SKU: ' + getRID();



	    var amount = prodSubtotal === 0.00 ? 0.01 : prodSubtotal;
		var calcAmt = prodSubtotal === 0.00 ? 0.01 : prodSubtotal * qty;
		var taxRate = parseFloat($('#liv-product-container').data('taxRate'));
		var tax = taxRate * calcAmt;
		var shipping = prodShipping;

		$('input[name="quantity"]').val(qty);

		$('input[name="item_name"]').val(itemName);


		$('input[name="amount"]').val(amount);
		$('input[name="tax_rate"]').val(taxRate * 100);
		//$('input[name="tax"]').val(tax);


		$('input[name="shipping"]').val(shipping);

		//set up handling fee
		var handling = Math.ceil(((0.029 * (calcAmt + tax + shipping) + 0.30) / (1 - 0.029)) * 100) / 100;
		//console.log(handling);
		if (prodSubtotal === 0) {
			handling = 0;
		}
		$('input[name="handling"]').val(handling);
		


		//get payment info - need 'full product name', 'price (adjusted for promo)',
		//'shipping', 'tax rate', 'if valid promo exists => add Promo: PROMOCODE'
		//to the paypal item description
		//paypal business Id, & paypal url
		$.ajax({
			url: '/api/payPalInfo',
			type: 'get',
			dataType: 'json',
			success: function(data) {
				//console.log(JSON.stringify(data));
				//update paypal form & submit
				if (!data.error) {
					
					$('input[name="business"]').val(data.paypalId);
					$('#paypal-form').attr('action', data.paypalUrl);
					$('#paypal-form').submit();

				}

			},
			error: function(jqXHR, status, error) {
				alert('An error occurred. Please try again later.');
			}
		});
	}

	function getNum(moneyString) {
		const rgx = /[$€£]+/g;
		return parseFloat(moneyString.replace(rgx, ''));
	}

	//Letter Checkout
	if (window.location.hash) {

		var h = window.location.hash.toLowerCase();

		if (h === '#creditcard') {
			$("html, body").delay(3000).animate({ scrollTop: $(document).height() }, 5000,
				"linear", function() {

				});
			stripeIt();
			
		}

		if (h === '#paypal') {
			paypalIt();
		}

	}


	function getAmt() { return '1' }

	function getRID() {
		var s = location.search;
		s = s.substring(1, s.length);
		var a = s.split('&');
		var rid = '';

		for(var i = 0; i < a.length; i++) {
			if (a[i].indexOf('rid') !== -1) {
				rid = a[i].split('=')[1];
				break;
			}
		}

		return rid;

	}



});