$(document).ready(function() {

	function showPaymentOpts() {

		$('body').addClass('liv-ltr-modal');
		$('#liv-ltr-modal-overlay').removeClass('liv-gone');
		$('#liv-ltr-pay-option').removeClass('liv-gone');

	}

	function hidePaymentOpts() {
		$('body').removeClass('liv-ltr-modal');
		$('#liv-ltr-modal-overlay').addClass('liv-gone');
		$('#liv-ltr-pay-option').addClass('liv-gone');
	}


	$('button.liv-ltr-btn-cta').click(function(e) {
		e.preventDefault();
		showPaymentOpts();
	});

	$('#liv-ltr-btn-modal-close').click(function(e) {
		e.preventDefault();
		hidePaymentOpts();
	});

	$('#liv-ltr-modal-overlay').click(function() {
		hidePaymentOpts();
	});

	if (location.search.length > 0) {
		var checkoutLinks = $('a.liv-ltr-btn-cta');

			$.each(checkoutLinks, function(i, l) {
				$(l).attr('href', $(l).attr('href') + location.search);
			});
		
	}

	//check for html 5 videos
	/*var vids = $('video');

	if (vids.length > 0) {

		$.each(vids, function(i, v) {
			$(v).bind('contextmenu',function() { return false; });
		});

	}*/



});