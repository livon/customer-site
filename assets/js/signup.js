$(document).ready(function() {


	var signupShown = false; //This may create a memory leak...watch out.


	function showSignup() {
		if($('#liv-signup').length) {
			$('body').addClass('liv-modal');
			$('#liv-modal-signup-bg').removeClass('liv-gone');
			$('#liv-signup').removeClass('liv-gone');
			signupShown = true;
		}
	}

	function hideSignup() {
		$('body').removeClass('liv-modal');
		$('#liv-modal-signup-bg').addClass('liv-gone');
		$('#liv-signup').addClass('liv-gone');
	}

	function showConfirmation() {
		$('#liv-signup-wrap-1').animate({top: '-600px'});
		$('#liv-signup-wrap-2').removeClass('liv-hidden');
		$('#liv-signup-wrap-2').animate({top: '-500px'});
	}

	function addEvent(obj, evt, fn) {
    if (obj.addEventListener) {
        obj.addEventListener(evt, fn, false);
    }
    else if (obj.attachEvent) {
        obj.attachEvent("on" + evt, fn);
    }
}


	$('.liv-signup-btn').click(function(e) {
		e.preventDefault();
		showSignup();
	});

	$('#liv-modal-signup-bg').click(function() {
		hideSignup();
	});

	$('.liv-signup-cancel').click(function(e) {
		e.preventDefault();
		hideSignup();
	});

	$('#liv-signup-submit').click(function(e) {
		e.preventDefault();

		var form = $('#liv-signup-form');

		$.ajax({
			url: '/recipes/signup',
			type: 'post',
			data: form.serialize(),
			success: function(data) {
				//alert(data);
				if (data === 'success') {
					//update signup form
					showConfirmation();
				}
			},
			error: function(jqXHR, status, error) {
				//alert(error);
			}
		});
	});


	addEvent(document, "mouseout", function(e) {
    e = e ? e : window.event;
    var from = e.relatedTarget || e.toElement;
    if (!from || from.nodeName == "HTML") {
        // stop your drag event here
        // for now we can just use an alert
        if (!signupShown) {
        	showSignup();
        }
    }
});

	//Mobile Support (no mouseout on mobile)

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent) ) {
    	
    	var signup = $('#liv-signup');
    	if (signup.length) {
	    	var popupDelay = $('#liv-signup').data('popuptime') || 20000; //default to 20s
	    	setTimeout(function() { showSignup(); }, popupDelay);
	    }
	}

});