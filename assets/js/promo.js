/****** Promotion Handler *********/

$(document).ready(function() {

	$('#liv-btn-add-promotion').click(function(e) {
		
		e.preventDefault();
		//check if
		addPromotion();
	});

	function addPromotion() {
		//get prodId and input promotion code
		const prodId = $('#liv-product-container').data('prodId');
		//console.log(prodId);
		var promoCode = $('#liv-promo-input').val();
		var csrf = $('#liv-product-container').data('csrf');

		$.ajax({
			url: '/checkout/addPromotion',
			type: 'post',
			datatype: 'json',
			data: { 'prodId': prodId, 'promoCode': promoCode, '_csrf' : csrf },
			success: function(promo) {
				console.log(JSON.stringify(promo));
				updatePromoView(promo);
			},
			error: function(jqXHR, status, message) {
				updatePromoView(false, true);
			}
			
		});
	}

	function updatePromoView(promo, err) {
		
		//check quantity

		$('.liv-promo').addClass('liv-gone');

		if (err) {

			$('#liv-promo-error').removeClass('liv-gone');
		} else {
			const taxRate = parseFloat($('#liv-product-container').data('taxRate'));
			const shipping = parseFloat($('#liv-product-container').data('shippingRate'));
			const origPrice = parseFloat($('#liv-price').data('origPrice'));
			var qty = parseInt($('#liv-qty').val());


			if (promo.isValid) {
				if (promo.isValid === true) {
					
					//refactor
					if (promo.type === 'shipping') {
						$('#liv-promo-success').removeClass('liv-gone');
						$('#liv-promo-desc').text(promo.desc);
						$('#liv-promo-desc').removeClass('liv-gone');
						$('#liv-shipping').text('$' + promo.price);
						$('#liv-shipping').addClass('liv-green');
						//update tax * fees
						var tax = origPrice * taxRate;
						//need to include fee
						var total = origPrice + tax + parseFloat(promo.price);
						var fee = (0.029 * total * qty + 0.30) / (1 - 0.029);
						total = total * qty + fee;
						tax *= qty;

						$('#liv-tax').text('$' + Math.ceil((tax + fee) * 100) / 100);
						$('#liv-total').text('$' + Math.ceil(total * 100) / 100);
						$('#liv-product-container').attr('data-promo-id', promo.id);
						$('#liv-product-container').attr('data-promo-type', promo.type);
						$('#liv-product-container').attr('data-promo-price', promo.price);
	
					} else if (promo.type === 'combo') {
						$('#liv-promo-success').removeClass('liv-gone');
						$('#liv-promo-desc').text(promo.desc);
						$('#liv-promo-desc').removeClass('liv-gone');
						


						$('#liv-shipping').text('$' + promo.combo.shipping.promoPrice);
						$('#liv-shipping').addClass('liv-green');

						var subtotal = origPrice * (1 - promo.combo.price.percentOff / 100);
						//update tax * fees
						var tax = taxRate * subtotal;
						//need to include fee
						var total = subtotal + promo.combo.shipping.promoPrice + tax;
						var fee = (0.029 * total * qty + 0.30) / (1 - 0.029);
						if (subtotal === 0) {
							fee = 0;
						}
						total = total * qty + fee;
						tax *= qty;

						var roundedSubtotal = Math.ceil(subtotal * 100) / 100;

						$('#liv-price').addClass('promo');
						$('#liv-promo-price').text('$' + roundedSubtotal);
						$('#liv-promo-price').removeClass('liv-gone');
						$('#liv-subtotal').text('$' + qty * roundedSubtotal);

						$('#liv-tax').text('$' + Math.ceil((tax + fee) * 100) / 100);
						$('#liv-total').text('$' + Math.ceil(total * 100) / 100);
						$('#liv-product-container').attr('data-promo-id', promo.id);
						$('#liv-product-container').attr('data-promo-type', promo.type);
						$('#liv-product-container').attr('data-promo-combo-percentoff', promo.combo.price.percentOff);
						$('#liv-product-container').attr('data-promo-combo-shipping', promo.combo.shipping.promoPrice);



					} else {

					$('#liv-promo-success').removeClass('liv-gone');
					$('#liv-shipping').removeClass('liv-green');
					//update subtotals and total. update img if it exists

					//need to include fee
					$('#liv-price').addClass('promo');
					$('#liv-promo-price').text('$' + promo.price);
					$('#liv-promo-price').removeClass('liv-gone');
					$('#liv-subtotal').text('$' + qty * promo.price);

					var tax = promo.price * taxRate; 
					var total = promo.price + tax + shipping;
					var fee = (0.029 * total + 0.30) / (1 - 0.029);
					if (promo.price === 0) {
						fee = 0;
					}
					total += fee;

					if (tax === 0) {
						$('#liv-tax').text('$' + Math.ceil(qty * tax * 100) / 100);	
					} else {
						$('#liv-tax').text('$' + Math.ceil(qty * (tax + fee) * 100) / 100);
					}

					
					$('#liv-total').text('$' + Math.ceil(qty * total * 100) / 100);
					$('#liv-product-container').attr('data-promo-id', promo.id);
					$('#liv-product-container').attr('data-promo-type', promo.type);
					$('#liv-product-container').attr('data-promo-price', promo.price);
				}
				
				    if (promo.img) {
						$('#liv-img-prod-details').attr('src', promo.img);
					}
				}
			} else {
				$('#liv-promo-invalid').removeClass('liv-gone');
				$('#liv-price').removeClass('promo');
				$('#liv-promo-price').addClass('liv-gone');
				
				//update subtotals and total and shipping
				//include fee
				$('#liv-subtotal').text('$' + origPrice * qty);

				var tax = origPrice * taxRate; 
				var total = origPrice + tax + shipping;
				var fee = (0.029 * total + 0.30) / (1 - 0.029);
				total += fee;

				$('#liv-tax').text('$' + Math.ceil(qty * (tax + fee) * 100) / 100);
				$('#liv-total').text('$' + Math.ceil(total * qty * 100) / 100);
				$('#liv-shipping').text('$' + shipping);
				$('#liv-shipping').removeClass('liv-green');
				$('#liv-product-container').attr('data-promo-id', '-1');
				$('#liv-product-container').attr('data-promo-type', 'na');
				$('#liv-img-prod-details').attr('src', $('#liv-img-prod-details').data('defImg'));
			}
		}

	}

});

