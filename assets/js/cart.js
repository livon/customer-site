/******* Cart Auxilliary Methods *******/

function getAllCartItems() {
	return JSON.parse(sessionStorage.getItem("livcart"));
}

function getCartItem(prodId) {
	var item = null;
	var cart = getAllCartItems();
	if (cart != null) {
		
		cart.forEach(function(itm) {
			if (itm.prodId === prodId.toString()) {
				item = itm;
			}
		});
	}

	return item;
}

function updateCartItem(prodId, prop, val) {
	var cart = getAllCartItems();
	if (cart === null) {
		console.error("Cart is Empty");
		return false;
	}

	var indx = -1;

	for(var i = 0; i < cart.length; i++) {
		if (prodId.toString() === cart[i].prodId) {
			indx = i;
			break;
		}
	}

	if (indx === -1) {
		console.error("Item not found in cart");
		return false;
	}

	cart[indx][prop] = val;

	sessionStorage.setItem("livcart", JSON.stringify(cart));

	return true;
}

function addCartItem(prodId, prodName, prodImg, prodUrl, flavorId, flavor, sizeId,
	size, price, qty, subtotal, inStock) {

	/*initialize the cart if it does not exist*/
	var cart = JSON.parse(sessionStorage.getItem("livcart")) || [];

	//check if item is already in cart - if so, increment the quantity by 1

	var existingItem = getCartItem(prodId);

	if(existingItem) {
		var qty = parseInt(existingItem.qty);
		qty++;
		//updateCartItem(prodId, 'qty', qty);
		updateQuantity(prodId, qty);
	} else {
		var item = {prodId: prodId, prodName: prodName, flvrId: flavorId,
			 	sizeId: sizeId, flvr: flavor, size: size, prodImg: prodImg,
			 	price: price, qty: qty, subtotal: subtotal, prodUrl: prodUrl,
			 	inStock: inStock };
		cart.push(item);
		sessionStorage.setItem("livcart", JSON.stringify(cart));
	}

	
}

function removeCartItem(prodId) {
	var cart = getAllCartItems();
	if (cart === null) {
		console.error("Cart is Empty");
		return false;
	}

	var indx = -1;

	for(var i = 0; i < cart.length; i++) {
		if (prodId.toString() === cart[i].prodId) {
			indx = i;
			break;
		}
	}

	if (indx === -1) {
		console.error("Item not found in cart");
		return false;
	}

	cart.splice(indx, 1);
	sessionStorage.setItem("livcart", JSON.stringify(cart));

	return true;
}

function updateQuantity(prodId, qty) {
	//update quantity & subtotal
	if (updateCartItem(prodId, 'qty', qty)) {

		var newSubtotal = Math.round(parseFloat(getCartItem(prodId).price) *  qty * 100) / 100;

		return updateCartItem(prodId, 'subtotal', newSubtotal);

	}

	return false;
}

function getCartCount() {
	var cart = getAllCartItems();

	var count = 0;

	if(cart) {

		cart.forEach(function(item) {
			count += +item.qty;
		});
	}	

	return count;
}

function logCartAdd(urlId, flavor,
	size, sizeId, qty) {

	$.ajax({
		url: '/products/cartAdd/',
		type: 'post',
		data: { name: urlId, flavor: flavor, size: size, sizeId: sizeId, qty: qty }

	});
}

/******* Document Loaded Script ********/
/* (Manages adding, updating, and removing items from the cart) */

$(document).ready(function() {

	redrawCart();

	//update the site nav cart icon to reflect the # of items in the cart

	//add event handler to Add to Cart button
	$('#liv-cart-submit').click(function(e) {

		//prevent regular form submission
		e.preventDefault();

		//get the properties of the item & add the item to the cart
		var prodId = $('#liv-product-container').data('prodId');
		var prodName = $('#liv-product-container').data('prodName');
		var prodImg = $('input[name="flavor"]:checked').data('img');
		var prodUrl = document.URL;
		//console.log(prodUrl);
		var flavorId = $('input[name="flavor"]:checked').val();
		var flavor = $('label[for="' + $('input[name="flavor"]:checked').attr('id') + '"]').text();
		var sizeId = $('#liv-size-select option:selected').val();
		var size = $('#liv-size-select option:selected').text();
		var price = $('#liv-size-select option:selected').data('price');
		var qty = 1;
		var subtotal = price;
		var inStock = $('#liv-product-container').data('inStock');

		prodId = prodId + flavorId + sizeId;

		//console.log('prod id:' + prodId);

		addCartItem(prodId, prodName, prodImg, prodUrl, flavorId, 
			flavor, sizeId, size, price, qty, subtotal, inStock);

		updateCartCounter();

		var a = prodUrl.split('/');

		logCartAdd(a[a.length - 1], flavor, size, sizeId, qty);

		//redirect to cart page;


	});

	function updateCartCounter() {
		var cartCount = getCartCount();
		var cartCounter = $('.liv-cart-count');

		cartCounter.text('(' + getCartCount() + ')');
		if (cartCount < 1 ) {
			cartCounter.addClass('liv-hidden');
		} else {
			cartCounter.removeClass('liv-hidden');
		}
	}

	function updateTotal() {
		var total = 0.00;

		$('.liv-cart-subtotal').each(function(){
			total += +$(this).text().substring(1, $(this).text().length);
		});

		total = Math.round(total * 100, 2) /100;

		$('.liv-cart-total').text('$' + total);
	}

	function redrawCart() {

		updateCartCounter();
		//check if cart table is present on page
		var cartTable = $('#liv-cart');
		var mbCart = $('#liv-cart-mb');

		if (cartTable) {//assume mobile cart exists

			//empty the cart table
			cartTable.find('tbody').remove();

		

			//empty the mobile cart
			mbCart.empty();

			//check if cart count > 0
			if (getCartCount() > 0) {

				var msg = 'Out of Stock';
				//hide the empty cart message
				$('#liv-empty-cart').addClass('liv-gone'); //handle mobile

				//reconstruct tbody & mobile cart items
				var tbody = $('<tbody></tbody>');

				var cartItems = getAllCartItems();

				cartItems.forEach(function(item) {

					var tr = $('<tr></tr>');
					var stockClass = item.inStock != false ? 'yes' : 'no';

					tr.append('<td><a href="' + item.prodUrl + '">' + '<img class="liv-cart-img" src="' + item.prodImg + '" /></a></td>');
					tr.append('<td class="name" data-prod-id="' + item.prodId + '"><p>' + item.prodName + '</p>'
								+ '<p class="liv-in-stock ' + stockClass + '">' + msg + '</p>' + '</td>');
					tr.append('<td><p>' + item.flvr + '</p></td>');
					tr.append('<td><p>' + item.size + '</p></td>');
					tr.append('<td><p>$' + item.price + '</p></td>');
					tr.append('<td class="center"><input class="liv-prod-qty" type="number" min="1" value="' + item.qty + '"/><button class="liv-btn-prod-update">Update</button></td>');
					tr.append('<td><p class="liv-cart-subtotal">$' + item.subtotal + '</p></td>');
					tr.append('<td><div class="liv-x">&#9587;</div></td>');

					tbody.append(tr);

					//create mobile cart item and append it to mbCart
					mbCart.append(createMobileCartItem(item.prodId, item.prodName, item.prodUrl,
						item.flvr, item.size, item.prodImg, item.price, item.qty, item.inStock));

				});

				cartTable.append(tbody);

				cartTable.removeClass('liv-gone');
				$('#liv-cart-container-mb').removeClass('liv-gone');




				//add click handlers for update and remove
				$('.liv-btn-prod-update').each(function() {
					$(this).click(function() {

						var qty = $(this).parent().find('.liv-prod-qty').first().val();
						var tr = $(this).closest('tr');
						var prodId = tr.find('td.name').data('prodId');

						if (+qty > 0) {
							updateQuantity(prodId, +qty);
						} else {
							removeCartItem(prodId);
						}
						
						redrawCart();


					});
				});


				$('.liv-x').each(function() {
					$(this).click(function() {

						//remove the item from the cart based on id
						//remove the row
						var tr = $(this).closest('tr');
						var prodId = tr.find('td.name').data('prodId');
						removeCartItem(prodId);
						//tr.remove();
						redrawCart();


					});
				});


				//repeat for mobile 

				$('.liv-btn-cart-item-mb-update').each(function() {
					$(this).click(function() {

						var qty = $(this).parent().find('.liv-cart-item-mb-qty').first().val();
						var item = $(this).closest('.liv-cart-item-mb');
						var prodId = item.data('prodId');

						if (+qty > 0) {
							updateQuantity(prodId, +qty);
						} else {
							removeCartItem(prodId);
						}
						
						redrawCart();


					});
				});


				$('.liv-x-mb').each(function() {
					$(this).click(function() {

						//remove the item from the cart based on id
						//remove the row
						var item = $(this).closest('.liv-cart-item-mb');
						var prodId = item.data('prodId');
						removeCartItem(prodId);
						//tr.remove();
						redrawCart();


					});
				});


				updateTotal();

			} else {
				//hide the table and show the empty cart message
				cartTable.addClass('liv-gone');
				$('#liv-cart-container-mb').addClass('liv-gone');
				$('#liv-empty-cart').removeClass('liv-gone');
			}
		}


		function createMobileCartItem(prodId, name, url, flvr, size, img, 
			price, qty, inStock) {

			var stockClass = inStock != false ? 'yes' : 'no';

			var cartItem = $('<div class="liv-cart-item-mb" data-prod-id="' + prodId + '""></div>');

			var topSection = $('<div class="liv-cart-item-mb-top-section"></div>');

			var topContent = $('<div class="liv-cart-item-mb-top-content"></div>');

				topContent.append('<h3 class="liv-cart-item-mb-name">' + name + '</h3>');
				topContent.append('<h4 class="liv-cart-item-mb-flvr">' + flvr + '</h4>');
				topContent.append('<h4 class="liv-cart-item-mb-size">' + size + '</h4>');

				topSection.append(topContent);
				topSection.append('<div class="liv-x-mb right">&#9587;</div>');

			var btmSection = $('<div class="liv-cart-item-mb-btm-section"></div>');

			var lftContent = $('<div class="liv-cart-item-mb-left-content"></div>');
				lftContent.append('<a href="' + url + '""><img class="liv-cart-item-mb-img liv-cart-img" src="' + img + '" /></a>');


			var rgtContent = $('<div class="liv-cart-item-mb-right-content"></div>');
				rgtContent.append('<h4 class="liv-cart-item-mb-price">$' + price + '</h4>');
				rgtContent.append('<h4>Quantity</h4>');
				rgtContent.append('<input class="liv-cart-item-mb-qty" type="number" min="1" value="' + qty + '" />');
				rgtContent.append('<button class="liv-btn-cart-item-mb-update">Update</button>');

				btmSection.append(lftContent);
				btmSection.append(rgtContent);

				cartItem.append(topSection);
				cartItem.append(btmSection);

				var msg = 'Sorry, this item is already out of stock. Please check back' +
	  			' in a few weeks and bear with us while we make more!';

				cartItem.append('<p class="liv-in-stock ' + stockClass + '">' + msg + '</p>');

				cartItem.append('<hr>');

				return cartItem;

		}

	}


});


