/****** Checkout Handler (Stripe Integration) *********/
var handler = StripeCheckout.configure({
  key: 'pk_test_qSlhwvT2OX1HXZlio1ufFpM6', //pk_live_kq2Ts4DqXugNGvVi7qVYbUcj | pk_test_qSlhwvT2OX1HXZlio1ufFpM6
  image: '/images/liv_on_checkout_logo.png', 
  locale: 'auto',
  token: function(token, args) {
  	//console.log('token called');
  	//console.log(JSON.stringify(args));
    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.
    $('#liv-img-processing').removeClass('liv-hidden');

    var prodId = $('#liv-product-container').data('prodId');
    var promoId = $('#liv-product-container').data('promoId');
    var qty = $('#liv-qty').val();
    var csrf = $('#liv-product-container').data('csrf');


    var form = $('<form action="/checkout/submitPayment" method="post"></form>');
    form.append('<input type="hidden" name="stripeToken" value="' + token.id + '"/>');
    form.append('<input type="hidden" name="prodId" value="' + prodId + '"/>');
    form.append('<input type="hidden" name="promoId" value="' + promoId+ '"/>');
    form.append('<input type="hidden" name="prodQty" value="' + qty + '"/>');
    form.append('<input type="hidden" name="billingName" value="' + args.billing_name + '"/>');
    form.append('<input type="hidden" name="billingAddress" value="' + args.billing_address_line1 + '"/>');
    form.append('<input type="hidden" name="billingCity" value="' + args.billing_address_city + '"/>');
    form.append('<input type="hidden" name="billingState" value="' + args.billing_address_state + '"/>');
    form.append('<input type="hidden" name="billingZip" value="' + args.billing_address_zip + '"/>');
    form.append('<input type="hidden" name="billingCountryCode" value="' + args.billing_address_country_code + '"/>');
    form.append('<input type="hidden" name="shippingName" value="' + args.shipping_name + '"/>');
    form.append('<input type="hidden" name="shippingAddress" value="' + args.shipping_address_line1 + '"/>');
    form.append('<input type="hidden" name="shippingCity" value="' + args.shipping_address_city + '"/>');
    form.append('<input type="hidden" name="shippingState" value="' + args.shipping_address_state + '"/>');
    form.append('<input type="hidden" name="shippingZip" value="' + args.shipping_address_zip + '"/>');
    form.append('<input type="hidden" name="shippingCountryCode" value="' + args.shipping_address_country_code + '"/>');
    form.append('<input type="hidden" name="_csrf" value="' + csrf + '"/>');


    $('body').append(form);

    form.submit();

  }
});

$(document).ready(function() {






	//Stripe

	$('#liv-btn-checkout').click(function(e) {
		e.preventDefault();

		stripeIt();
	});


// Close Stripe Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});

	
	//PayPal

$('#paypal-submit').click(function(e) {
		e.preventDefault();

		paypalIt();

		
	});


	function stripeIt() {
		const prodName = $('#liv-product-container').data('prodName');
		var prodPrice = $('#liv-total').text();
		prodPrice = prodPrice.substring(1, prodPrice.length);
		prodPrice = parseFloat(prodPrice) * 100;
		if (prodPrice === 0) {
			prodPrice = 50; //$0.50 Stripe's minimum fee
		}

		 handler.open({
    		name: 'Liv On',
    		description: prodName, //name of product
    		amount: prodPrice, //price
    		billingAddress: true,
    		shippingAddress: true,
    		zipCode: true
  		});
	}

	function paypalIt() {
		var prodId = $('#liv-product-container').data('prodId');
    	var promoId = $('#liv-product-container').data('promoId');

    	var url = '/checkout/paymentInfo/' + prodId;
    	if (promoId != '-1' && promoId != -1) { 
    		url += '?promo=true&promoId=' + promoId;
    	}

		//get payment info - need 'full product name', 'price (adjusted for promo)',
		//'shipping', 'tax rate', 'if valid promo exists => add Promo: PROMOCODE'
		//to the paypal item description
		//paypal business Id, & paypal url
		$.ajax({
			url: url,
			type: 'get',
			dataType: 'json',
			success: function(data) {
				//console.log(JSON.stringify(data));
				//update paypal form & submit
				if (!data.error) {
					var itemName = data.prodName + ' - ' + data.prodSize;
					var qty = $('#liv-qty').val();
					if (qty < 1) qty = 1;

					if (data.promoCode != '-1') {
						itemName += ' - promo: ' + data.promoCode + ' - ' + data.promoDesc;
					}

					itemName += ' | Qty: ' + qty;

					$('input[name="quantity"]').val(qty);

					$('input[name="item_name"]').val(itemName);

					var amount = parseFloat(data.subtotal) === 0.00 ? 0.01 : data.subtotal;
					var calcAmt = parseFloat(data.subtotal) === 0.00 ? 0.01 : data.subtotal * qty;
					var tax = parseFloat(data.taxRate) * calcAmt;
					var shipping = parseFloat(data.shipping);


					$('input[name="amount"]').val(amount);
					$('input[name="tax_rate"]').val(data.taxRate * 100);
					//$('input[name="tax"]').val(tax);

					if (data.promoCode && data.promoType === 'shipping') {
						shipping = parseFloat(data.promoPrice);
								
					}

					/*console.log(data.taxRate);
					console.log(amount);
					console.log(tax);
					console.log(shipping);*/
		
					$('input[name="shipping"]').val(shipping);

					//set up handling fee
					var handling = Math.ceil(((0.029 * (calcAmt + tax + shipping) + 0.30) / (1 - 0.029)) * 100) / 100;
					//console.log(handling);
					if (data.subtotal === 0) {
						handling = 0;
					}
					$('input[name="handling"]').val(handling);
					
					$('input[name="business"]').val(data.paypalId);
					$('#paypal-form').attr('action', data.paypalUrl);
					$('#paypal-form').submit();

				}

			},
			error: function(jqXHR, status, error) {
				alert('An error occurred. Please try again later.');
			}
		});
	}

	//Letter Checkout
	if (window.location.hash) {

		var h = window.location.hash.toLowerCase();

		if (h === '#creditcard') {
			$("html, body").delay(3000).animate({ scrollTop: $(document).height() }, 5000,
				"linear", function() {

				});
			stripeIt();
			
		}

		if (h === '#paypal') {
			paypalIt();
		}

	}


	function getAmt() { return '1' }



});