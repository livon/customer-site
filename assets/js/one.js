/******* One-Product Checkout ********/

$(document).ready(function() {

	//adjust the price when the quantity changes
	//if the new value is < 1 then set the value to 1
	$('#liv-qty').on('input', function() {
		//console.log($(this).val());
		//update price values
		if ($(this).val() >= 1) {
			updatePriceView($(this).val());
		} else {
			$(this).val(1);
			updatePriceView($(this).val());
		}
	});

	$('#liv-qty-x').on('input', function() {
		//console.log($(this).val());
		//update price values
		if ($(this).val() >= 1) {
			updatePriceView($(this).val(), true);
		} else {
			$(this).val(1);
			updatePriceView($(this).val(), true);
		}
	});

	function updatePriceView(qty, isExp) {

		//depends on whether or not promo is active => check promo id & promo type
		var promoId = $('#liv-product-container').attr('data-promo-id');
		var promoType = $('#liv-product-container').attr('data-promo-type');

		//console.log(promoId);
		//console.log(promoType);

		if (promoId != '-1' && promoId != -1) {

			if (promoType === 'shipping') {

				//console.log('shipping');

				var subtotal = qty * getMoneyFloat($('#liv-price').data('origPrice'));
				var tax = getMoneyFloat($('#liv-product-container').data('taxRate') * subtotal);
				var shipping = getMoneyFloat($('#liv-product-container').data('promoPrice')); //promo price
				var total = subtotal + shipping + tax;
				var fee = getFeeAmt(total);
				total += fee;

				$('#liv-subtotal').text('$' + subtotal);
				$('#liv-tax').text('$' + Math.ceil((tax + fee) * 100) / 100);
				$('#liv-total').text('$' + Math.ceil(total * 100) / 100);

			} else if (promoType === 'combo') {
				var discount = parseFloat($('#liv-product-container').data('promoComboPercentoff'));
				var subtotal = qty * getMoneyFloat($('#liv-price').data('origPrice')) * (1 - discount / 100) ; //promo price
				//console.log(discount);
				//console.log(subtotal);
				var tax = getMoneyFloat($('#liv-product-container').data('taxRate') * subtotal);
				var shipping = getMoneyFloat($('#liv-product-container').data('promoComboShipping')); 
				var total = subtotal + shipping + tax;
				var fee = getFeeAmt(total);
				total += fee;

				$('#liv-subtotal').text('$' + subtotal);
				$('#liv-tax').text('$' + Math.ceil((tax + fee) * 100) / 100);
				$('#liv-total').text('$' + Math.ceil(total * 100) / 100);
			} else {

				var subtotal = qty * getMoneyFloat($('#liv-product-container').data('promoPrice')); //promo price
				var tax = getMoneyFloat($('#liv-product-container').data('taxRate') * subtotal);
				var shipping = getMoneyFloat($('#liv-product-container').data('shippingRate')); 
				var total = subtotal + shipping + tax;
				var fee = getFeeAmt(total);
				total += fee;

				$('#liv-subtotal').text('$' + subtotal);
				$('#liv-tax').text('$' + Math.ceil((tax + fee) * 100) / 100);
				$('#liv-total').text('$' + Math.ceil(total * 100) / 100);
			}
		} else {
			//console.log('error');
			var subtotal, tax, shipping;

			if (isExp) {
				var price = getMoneyFloat($('#liv-promo-price-x').text());

				if (!price) {
					price = getMoneyFloat($('#liv-price').text());
				}

				subtotal = qty * price;
			    tax = getMoneyFloat($('#liv-product-container').data('taxRate') * subtotal);
			    shipping = getMoneyFloat($('#liv-shipping').text());
			} else {
				subtotal = qty * getMoneyFloat($('#liv-price').data('origPrice'));
			    tax = getMoneyFloat($('#liv-product-container').data('taxRate') * subtotal);
			    shipping = getMoneyFloat($('#liv-product-container').data('shippingRate'));
			}

			var total = subtotal + shipping + tax;
			var fee = getFeeAmt(total);
			total += fee;

			$('#liv-subtotal').text('$' + subtotal);
			$('#liv-tax').text('$' + Math.ceil((tax + fee) * 100) / 100);
			$('#liv-total').text('$' + Math.ceil(total * 100) / 100);
		}
	}

	



});

function getFeeAmt(total) {

	//return Math.ceil(((total + 0.30) / (1 - 0.029) - total) * 100) / 100; 
	return (total + 0.30) / (1 - 0.029) - total;
}

function getMoneyFloat($text) {

		if (typeof $text === 'string') {
			var indx = $text.indexOf('$');

			if (indx !== -1) {
				return parseFloat($text.substring(0,indx) + $text.substring(indx + 1, $text.length)); //removed * 100 => need to test this on regular site
			} else {
				return parseFloat($text);
			}

		} else if (typeof $text === 'number') {
			return $text;
		} else {
			return undefined;
		}
	}
