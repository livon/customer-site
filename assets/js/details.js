/****** Product Details *********/

$(document).ready(function() {

	//on radio button change, get checked radio button
	//find its data img source and load the image
	$('input[name="flavor"]').each(function() {
		$(this).change(function() {
			if ($(this).is(':checked')) {
				//console.log($(this).data('img'));
				$('#liv-img-prod-details').attr('src', $(this).data('img'));
			}
			
	
		});		
	});

	$('#liv-size-select').change(function() {
		$('#liv-price').text('$' + $('#liv-size-select option:selected').data('price'));
	});

});